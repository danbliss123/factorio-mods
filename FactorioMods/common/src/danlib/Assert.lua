
Assert = {}

function Assert.assertEquals(failures, message, expected, actual)
	if expected ~= actual then
		table.insert(failures,"Equality check failure: "..message..": expected "..expected..", got "..actual)
	end
end

function Assert.assertTrue(failures, message, boolean)
	if not boolean then
		table.insert(failures,"Boolean check failure: "..message)
	end
end

function Assert.assertGreaterThan(failures, message, highValue, lowValue)
	if highValue <= lowValue then
		table.insert(failures,"Comparison check failure: "..message..": "..highValue.." > "..lowValue)
	end
end
