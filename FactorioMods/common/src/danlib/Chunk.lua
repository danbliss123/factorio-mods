
-- Chunks should have { x=xChunkCoordinate, y=yChunkCoordinate }
-- (-2,-2) (-1,-2) | ( 0,-2) ( 1,-2) 
-- (-2,-1) (-1,-1) | ( 0,-1) ( 1,-1) 
-- --------------origin-------------
-- (-2, 0) (-1, 0) | ( 0, 0) ( 1, 0) 
-- (-2, 1) (-1, 1) | ( 0, 1) ( 1, 1) 

Chunk = {x=0, y=0}

Chunk.chunksize = 32


function Chunk.fromArea(area)
	return Chunk.fromLocation(Chunk.areaToCenter(area))
end

function Chunk.fromLocation(loc)
	return {x=math.floor(loc.x/Chunk.chunksize), y=math.floor(loc.y/Chunk.chunksize)}
end

function Chunk.toArea(chunk)
	local left_top={x=chunk.x*Chunk.chunksize, y=chunk.y*Chunk.chunksize}
	return { left_top=left_top, right_bottom={x=left_top.x+Chunk.chunksize, y=left_top.y+Chunk.chunksize} }
end

function Chunk.toCenter(chunk)
	return {x=chunk.x*Chunk.chunksize+Chunk.chunksize/2, y=chunk.y*Chunk.chunksize+Chunk.chunksize/2}
end

function Chunk.toTopLeft(chunk)
	return {x=chunk.x*Chunk.chunksize, y=chunk.y*Chunk.chunksize}
end

function Chunk.areaToCenter(area)
	return {x=(area.left_top.x+area.right_bottom.x)/2, y=(area.left_top.y+area.right_bottom.y)/2}
end

function Chunk.toIdString(chunk)
	return "(x="..chunk.x..",y="..chunk.y..")"
end

function Chunk.toString(chunk)
	return "Chunk x="..chunk.x.." y="..chunk.y
end


-- Lumps are like chunks but with a size of 8 insted of 32.  Each Chunk is composed of 16 lumps

Lump = {x=0, y=0}

Lump.lumpSize = 8


function Lump.fromArea(area)
	return Lump.fromLocation(Chunk.areaToCenter(area))
end

function Lump.fromLocation(loc)
	return {x=math.floor(loc.x/Lump.lumpSize), y=math.floor(loc.y/Lump.lumpSize)}
end

function Lump.toArea(lump)
	local left_top={x=lump.x*Lump.lumpSize, y=lump.y*Lump.lumpSize}
	return { left_top=left_top, right_bottom={x=left_top.x+Lump.lumpSize, y=left_top.y+Lump.lumpSize} }
end

function Lump.toCenter(lump)
	return {x=lump.x*Lump.lumpSize+Lump.lumpSize/2, y=lump.y*Lump.lumpSize+Lump.lumpSize/2}
end

function Lump.chunk(lump)
	return {x = math.floor(lump.x/4), y = math.floor(lump.y/4)}
end

function Lump.toString(lump)
	return "Lump x="..lump.x.." y="..lump.y
end

function Chunk.lumps(chunk)
	local left = chunk.x*4
	local top = chunk.y*4
	local lumpList = {}
	for x = 1,4,1 do
		lumpList[x] = {}
		for y = 1,4,1 do
			lumpList[x][y] = {x=left+x-1, y=top+y-1}
		end
	end
	return lumpList
end


-- Cells are groups of chunks that are larger proportionally to their distance from the origin.  The starting area is also a cell.
-- Cells should have {ring = the ring they are in, cellnum = a number identifying the cell in the ring}
-- Cells are always square
-- ring 0 is always the starting area.  It should have a cellnum of 0 as well.
-- ring 1's outside edge is a square with dimensions twice those of the starting area.  Its inside edge is the starting area
-- ring 2 is twice ring 1, etc...
-- cellnums:
--  1  2  3  4
-- 12        5
-- 11        6
-- 10  9  8  7

Cell = {ring=0, cellnum=0}

-- sarc = starting area radius chunks
Cell.sarc = 8
Cell.halfsarc = Cell.sarc/2
Cell.count = 12


-- returns {ring=, cellnum=}
function Chunk.toCell(chunk)
	local x = math.abs(chunk.x+.5)
	local y = math.abs(chunk.y+.5)
	local ring = 0
	while x>Cell.sarc or y>Cell.sarc do
		ring = ring+1
		x = x/2
		y = y/2
	end
	if chunk.x<0 then x = -x end
	if chunk.y<0 then y = -y end
	if ring==0 then
		return {ring=0, cellnum=1}
	else
		local cell = 0
		if y<=-Cell.halfsarc then
			cell = math.ceil((x+Cell.sarc)/Cell.halfsarc)
		elseif x>=Cell.halfsarc then
			cell = math.ceil((y+Cell.sarc)/Cell.halfsarc)+3
		elseif y>=Cell.halfsarc then
			cell = math.ceil((Cell.sarc-x)/Cell.halfsarc)+6
		elseif x<=-Cell.halfsarc then
			cell = math.ceil((Cell.sarc-y)/Cell.halfsarc)+9
		end
		return {ring=ring, cellnum=cell}
	end
end

function Cell.topLeftChunk(cell)

	if cell.ring==0 then
		return {x=-Cell.sarc, y=-Cell.sarc}
	end
	
	local unscaledTL = {x=0,y=0}
	if cell.cellnum <= 4 then
		unscaledTL.x = (cell.cellnum-1)*Cell.halfsarc-Cell.sarc
		unscaledTL.y = -Cell.sarc
	elseif cell.cellnum <= 7 then
		unscaledTL.x = Cell.halfsarc
		unscaledTL.y = (cell.cellnum-4)*Cell.halfsarc-Cell.sarc
	elseif cell.cellnum <= 10 then
		unscaledTL.x = (10-cell.cellnum)*Cell.halfsarc-Cell.sarc
		unscaledTL.y = Cell.halfsarc
	else
		unscaledTL.x = -Cell.sarc
		unscaledTL.y = (13-cell.cellnum)*Cell.halfsarc-Cell.sarc
	end
	
	local scale = math.pow(2,cell.ring)
	
	return {x=unscaledTL.x*scale, y=unscaledTL.y*scale}
end

function Cell.toArea(cell)
	if cell.ring==0 then
		local radius = Cell.sarc*Chunk.chunksize
		return {
			left_top = {x=-radius, y=-radius},
			right_bottom = {x=radius, y=radius} 
		}
	end
	
	local tl = Chunk.toTopLeft(Cell.topLeftChunk(cell))
	local width = Cell.width(cell)
	return {
		left_top = tl,
		right_bottom = {x=tl.x+width, y=tl.y+width} 
	}
end

function Cell.toCenter(cell)
	if cell.ring==0 then
		return {x=0, y=0}
	end
	
	local tl = Chunk.toTopLeft(Cell.topLeftChunk(cell))
	local width = Cell.width(cell)
	return {x=tl.x+width/2, y=tl.y+width/2} 
end

function Cell.toTopLeft(cell)
	if cell.ring==0 then
		local radius = Cell.sarc*Chunk.chunksize
		return {x=-radius, y=-radius}
	end
	return Chunk.toTopLeft(Cell.topLeftChunk(cell))
end

function Cell.widthChunks(cell)
	if cell.ring==0 then
		return Cell.sarc*2
	end
	local width = math.pow(2,cell.ring) * Cell.halfsarc
	return width
end

function Cell.width(cell)
	if cell.ring==0 then
		return Cell.sarc*2*Chunk.chunksize
	end
	return Cell.widthChunks(cell)*Chunk.chunksize
end

function Cell.distanceFromOrigin(cell)
	if cell.ring==0 then
		return 0
	end
	local width = math.pow(2,cell.ring) * Cell.sarc
	return width
end	

function Cell.toString(cell)
	return "Cell ring="..cell.ring.." cellnum="..cell.cellnum
end










