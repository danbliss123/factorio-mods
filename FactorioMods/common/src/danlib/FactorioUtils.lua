
function wall(mod, message)
	if not message then
		message = mod
		mod = "Unknown"
	end
	local fullmessage = "("..mod..") "..message
	for _,player in pairs(game.players) do
		player.print(fullmessage)
	end
end

function deepcopy(orig)
    local orig_type = type(orig)
    local copy
    if orig_type == 'table' then
        copy = {}
        for orig_key, orig_value in next, orig, nil do
            copy[deepcopy(orig_key)] = deepcopy(orig_value)
        end
        setmetatable(copy, deepcopy(getmetatable(orig)))
    else -- number, string, boolean, etc
        copy = orig
    end
    return copy
end

function cloneEntityPrototype(original, name, tint)
	local p = table.deepcopy(original)
	p.name = name
	if p.minable and p.minable.result then
		p.minable.result = name
	end
	if p.place_result then
		p.place_result = name
	end
	if p.result then
		p.result = name
	end
	if p.take_result then
		p.take_result = name
	end
	if tint then
  		if p.icon then
			if not p.icons then p.icons = {} end
  			table.insert(p.icons, {icon=p.icon})
  			p.icon = nil
  		end
  		if p.icons then
	  		for _,icon in ipairs(p.icons) do
	  			icon.tint = tint
	  		end
	  	end
  		if p.pictures then
  			if p.pictures.filename then
  				p.pictures.tint = tint
  			end
  			for _,picture in ipairs(p.pictures) do
  				if type(picture)=="table" and picture.filename then
  					picture.tint = tint
  				end
  			end
  			for _,picture in pairs(p.pictures) do
  				if type(picture)=="table" and picture.filename then
  					picture.tint = tint
  				end
  			end
  		end
	end
	return p
end
























