Map={}

function Map.makeMapRaster (radius)
	local xpmData = {}
	
	local rasterWidth=radius*2
	for r = 1,rasterWidth,1 do
		xpmData[r] = {}
		local gy = r-radius-.5
		local absgy = math.abs(gy)
		for c = 1,rasterWidth,1 do
			local gx = c-radius-.5
			local absgx = math.abs(gx)
			local largest = absgy
			if absgx>absgy then largest = absgx end
			if largest < 256 then
				if math.floor(gx/32)%2==math.floor(gy/32)%2 then
					xpmData[r][c] = 2
				else
					xpmData[r][c] = 1
				end
			elseif largest < 512 then
				xpmData[r][c] = 2
			elseif largest < 1024 then
				xpmData[r][c] = 1
			elseif largest < 2048 then
				xpmData[r][c] = 2
			elseif largest < 4096 then
				xpmData[r][c] = 1
			elseif largest < 8192 then
				xpmData[r][c] = 2
			else
				xpmData[r][c] = 1
			end
		end
	end
	return xpmData
end
