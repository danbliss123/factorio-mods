
XpmLib = {}

XpmLib.charSet = {
	"_","0","1","2","3","4","5","6","7","8","9",
	"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z",
	"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"
}

XpmLib.grouping = 2000


XpmData = { raster = {}, colors = {} }



-- xpmData - should be {raster=rectangular array of arrays of color indicies, colors=array of hex color codes}
function XpmLib.write(xpmData, writelnToFileFunction)
	local height = #xpmData.raster
	local width = #xpmData.raster[1]
	local colorCount = #xpmData.colors

	writelnToFileFunction("/* XPM */\n")
	writelnToFileFunction("static char * XFACE[] = {\n")
	writelnToFileFunction("\""..table.concat({width, height, colorCount, 1}, " "))
	for index,color in ipairs(xpmData.colors) do
		local char = XpmLib.charSet[index]
		writelnToFileFunction("\",\n\""..char.." c #"..color)
	end
	for r,row in ipairs(xpmData.raster) do
		local firstGroup = true
		for bigC = 0,#row-1,XpmLib.grouping do
			local group = {}
			if firstGroup then
				firstGroup = false
				table.insert(group, "\",\n\"")
			end
			local groupLen = XpmLib.grouping
			if #row-bigC<groupLen then groupLen = #row-bigC end
			for c = 1,groupLen,1 do
				table.insert(group, XpmLib.charSet[row[bigC+c]] )
			end
			writelnToFileFunction(table.concat(group))
		end
	end
	writelnToFileFunction("\",\n};\n")

end

function XpmLib.writeToFile(xpmData, fileName)
	local file = io.open(fileName,"w")
	XpmLib.write(xpmData,function(text)
		file:write(text)
	end)
	file:close()
end


