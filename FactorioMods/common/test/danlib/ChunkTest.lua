require "danlib.Chunk"
require "danlib.Assert"
require "test.Test"


ChunkTest = {

	testChunks = ParameterizedTest.new({
		values={
			{x=-43, y=-6 },  {x=-4, y=-6 },  {x=-1, y=-6 },  {x=0, y=-6 },  {x=5, y=-6 },  {x=54, y=-6 },
			{x=-42, y=-34},  {x=-4, y=-34},  {x=-1, y=-34},  {x=0, y=-34},  {x=3, y=-34},  {x=36, y=-34},
			{x=-42, y=-1 },  {x=-4, y=-1 },  {x=-1, y=-1 },  {x=0, y=-1 },  {x=6, y=-1 },  {x=65, y=-1 },
			{x=-77, y=0  },  {x=-7, y=0  },  {x=-1, y=0  },  {x=0, y=0  },  {x=2, y=0  },  {x=23, y=0  },
			{x=-29, y=15 },  {x=-2, y=15 },  {x=-1, y=15 },  {x=0, y=15 },  {x=4, y=15 },  {x=42, y=15 },
			{x=-74, y=54 },  {x=-7, y=54 },  {x=-1, y=54 },  {x=0, y=54 },  {x=7, y=54 },  {x=77, y=54 },
			
			{x=-24, y=-24 },  {x=-24, y=-8 },  {x=-24, y=8 },  {x=-24, y=24 },  
			{x= -8, y=-24 },  									{x= -8, y=24 },  
			{x=  8, y=-24 },  									{x=  8, y=24 },  
			{x= 24, y=-24 },  {x= 24, y=-8 },  {x= 24, y=8 },  {x= 24, y=-24 },  
		},
		
		testToAreaAndBack = function(chunk)
			local failures = {}
			local area = Chunk.toArea(chunk)
			local chunk2 = Chunk.fromArea(area)
			Assert.assertEquals(failures,"Chunk x",chunk.x,chunk2.x)
			Assert.assertEquals(failures,"Chunk y",chunk.y,chunk2.y)
			return failures
		end,
	
		testToCenterAndBack = function(chunk)
			local failures = {}
			local center = Chunk.toCenter(chunk)
			for _,x in ipairs({-15.9,0,15.9}) do
				for _,y in ipairs({-15.9,0,15.9}) do
					local chunk2 = Chunk.fromLocation({x=center.x+x, y=center.y+y})
					Assert.assertEquals(failures,"Chunk x "..x..","..y,chunk.x,chunk2.x)
					Assert.assertEquals(failures,"Chunk y "..x..","..y,chunk.y,chunk2.y)
				end
			end
			return failures
		end,
		
		testToCell = function(chunk)
			local failures = {}
			local cell = Chunk.toCell(chunk)
			local cellArea = Cell.toArea(cell)
			local chunkCenter = Chunk.toCenter(chunk)
			Assert.assertGreaterThan(failures,"X Left",chunkCenter.x,cellArea.left_top.x)
			Assert.assertGreaterThan(failures,"X Right",cellArea.right_bottom.x,chunkCenter.x)
			Assert.assertGreaterThan(failures,"Y Top",chunkCenter.y,cellArea.left_top.y)
			Assert.assertGreaterThan(failures,"Y Bottom",cellArea.right_bottom.y,chunkCenter.y)
			return failures
		end,
		
		testLumps = function(chunk)
			local failures = {}
			local lumps = Chunk.lumps(chunk)
			local lumpCount = 0;
			for _a,lumpRow in ipairs(lumps) do
				for _b,lump in ipairs(lumpRow) do
					local lumpArea = Lump.toArea(lump)
					local lump2 = Lump.fromArea(lumpArea)
					local chunk2 = Chunk.fromArea(lumpArea)
					local chunk3 = Lump.chunk(lump)
					Assert.assertEquals(failures,"Lump x",lump.x,lump2.x)
					Assert.assertEquals(failures,"Lump y",lump.y,lump2.y)
					Assert.assertEquals(failures,"Chunk2 x",chunk.x,chunk2.x)
					Assert.assertEquals(failures,"Chunk2 y",chunk.y,chunk2.y)
					Assert.assertEquals(failures,"Chunk3 x",chunk.x,chunk3.x)
					Assert.assertEquals(failures,"Chunk3 y",chunk.y,chunk3.y)
					lumpCount = lumpCount+1
				end
			end
			Assert.assertEquals(failures,"Number of lumps",16,lumpCount)
			return failures
		end
	

	})

}





