
Test = {}

ParameterizedTest = {
	testType = "parameterized",
	values = {}
}

ParameterizedTest.__index = ParameterizedTest

function ParameterizedTest.new(obj)
	obj = obj or {}
	setmetatable(obj, ParameterizedTest)
	return obj
end


function Test.performTest(name, test)
	local failures
	local fullName = name
	if type(test)=="function" then
		failures = test()
	elseif test.testType=="parameterized" then
		failures = {}
		local smallTestNames = {}
		for smallName, smallTest in pairs(test) do
			if smallName=="values" then
				-- do nothing
			elseif type(smallTest)=="function" then
				table.insert(smallTestNames, smallName)
			else
				table.insert(failures,"Unknown parameterized test type "..name.."."..smallName)
			end
		end
		
		fullName = name.." ("..table.concat(smallTestNames, ",")..") x "..#(test.values) 
		
		for index, value in pairs(test.values) do
			if #failures>0 then
				break
			end
			for smallIndex, smallName in ipairs(smallTestNames) do
				local smallFailures = test[smallName](value)
				for _,smallFailure in ipairs(smallFailures) do
					table.insert(failures, smallName.."["..index.."]: "..smallFailure)
				end
				if #failures>0 then
					break
				end
			end
		end
	else
		failures = {"Unknown test type for "..name}
	end
	
	if #failures>0 then
		print("FAIL "..fullName)
		print("    "..table.concat(failures, "\n    ").."\n")
	else
		print("PASS "..fullName)
	end
end


function Test.testAll(overallName, tests)
	print("--------------------------------------------------------------------------------")
	print("Testing "..overallName)
	for name, test in pairs(tests) do
		Test.performTest(name, test)
	end
end

