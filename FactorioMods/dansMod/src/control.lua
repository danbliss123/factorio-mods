require "img.XpmLib"
require "danlib.Chunk"
require "danlib.Map"

logtwo = math.log(2)
radradtwo=math.sqrt(math.sqrt(2))


logfile="dansMod.txt"


function wall(message)
	local fullmessage = "(DansMod) "..message
	for _,player in pairs(game.players) do
		player.print(fullmessage)
	end
end

resourceList = nil

function getResourceList()
	if not resourceList then
		resourceList={}
		for name, ep in pairs(game.entity_prototypes) do
			if ep.type=="resource" then
				table.insert(resourceList, name)
				if ep.infinite_resource then
					table.insert(resourceList, name.."-count")
				end
			end
		end
	end
	return resourceList
end

-- dictionary of resourceName = {index=, hex=}
function createColorNumberMap(firstIndex)

	local cnm = { water={index=firstIndex, hex="3060FF"} }
	local index = firstIndex+1
	for _,name in ipairs(getResourceList()) do
		local res = game.entity_prototypes[name]
		if res then
			cnm[name] = {index=index, hex=hexByte(res.map_color.r)..hexByte(res.map_color.g)..hexByte(res.map_color.b)}
		end
		index = index+1
	end
	
	return cnm
end

analysis = {
	active = false,
	surface = {},
	radius = 0,
	colorData={},
	xpmData = {},
	nextLineToScan = 0,
	resourceChart = {}
}

scanlevels = {
	{radius=256},
	{radius=512},
	{radius=1024},
	{radius=2048},
	{radius=4096},
	{radius=8192}
}

for _,scanlevel in ipairs(scanlevels) do
	scanlevel.expected = (scanlevel.radius/32)*(scanlevel.radius/32)*4
	scanlevel.actual=0
end


function startAnalysis(surface, radius)
	wall("Starting analysis for radius "..radius)
	analysis.active = true
	analysis.surface = surface
	analysis.radius = radius
	analysis.colorData = createColorNumberMap(3)
	analysis.xpmData = Map.makeMapRaster(radius+1)
	analysis.nextLineToScan = -radius
	analysis.resourceChart = {}
	for _,name in ipairs( getResourceList() ) do
		analysis.resourceChart[name] = {}
		for i = 1,math.ceil(radius*math.sqrt(2)/32),1 do
			analysis.resourceChart[name][i]=0
		end
	end
end

function continueAnalysis()
	if analysis.nextLineToScan >= analysis.radius then
		endAnalysis()
		return
	end
	local searchArea = {
			top_left={x=-analysis.radius, y=analysis.nextLineToScan}, 
			bottom_right={x=analysis.radius, y=analysis.nextLineToScan+32}}
	local resources = analysis.surface.find_entities_filtered( {
		area=searchArea, type='resource'} )
	for _,res in ipairs(resources) do
		local x = res.position.x
		local y = res.position.y
		if x>searchArea.top_left.x and x<searchArea.bottom_right.x and y>searchArea.top_left.y and y<searchArea.bottom_right.y then 
			analysis.xpmData[math.floor(y)+analysis.radius+2][math.floor(x)+analysis.radius+2] = 
				analysis.colorData[res.name].index;
			local d = math.ceil(math.sqrt(x*x+y*y)/32)
			analysis.resourceChart[res.name][d] = analysis.resourceChart[res.name][d]+res.amount
			if res.prototype.infinite_resource then
				analysis.resourceChart[res.name.."-count"][d] = analysis.resourceChart[res.name.."-count"][d]+1
			end
		end
	end
	for r = analysis.nextLineToScan,analysis.nextLineToScan+32,1 do
		for c = -analysis.radius,analysis.radius-1,1 do
			local tile = analysis.surface.get_tile(c+.5,r+.5)
			if tile.collides_with("water-tile") then
				analysis.xpmData[r+analysis.radius+2][c+analysis.radius+2] = analysis.colorData["water"].index
			end
		end
	end
	analysis.nextLineToScan = analysis.nextLineToScan+32
	collectgarbage("collect")
end

function endAnalysis()
	analysis.active = false
	local xpmColorTable = {"FFFFFF", "DDDDDD"}
	local i=3
	for resource, colorinfo in pairs(analysis.colorData) do
		xpmColorTable[i] = 1
		i=i+1
	end
	for resource, colorinfo in pairs(analysis.colorData) do
		xpmColorTable[colorinfo.index] = colorinfo.hex
	end
	
	game.write_file("my.xpm", "", false)
	
	XpmLib.write({ raster=analysis.xpmData, colors=xpmColorTable }, function(line)
		game.write_file("my.xpm", line, true)
	end)
	
	game.write_file("my.csv", "", false)
	
--	game.write_file("my.csv", table.concat({"name","color",
--		"rich_base","rich_mult","min_ra","norm_ra"}, ",")..",", true)
	
	local columns = {"name"}
	for i = 4,analysis.radius/32,1 do
		table.insert(columns, i*32)
	end
	
	game.write_file("my.csv", table.concat(columns, ",").."\n", true)
	
	for name,chart in pairs(analysis.resourceChart) do
		local ep = game.entity_prototypes[name]
		local chartRow = {name}
		local total=0
		
--		table.insert(chartRow, ep.autoplace_specification.richness_base)
--		table.insert(chartRow, ep.autoplace_specification.richness_multiplier)
--		table.insert(chartRow, ep.minimum_resource_amount)
--		table.insert(chartRow, ep.normal_resource_amount)
		
		for i = 1,analysis.radius/32,1 do
			total = total+chart[i]
			if i>=4 then
				table.insert(chartRow,total)
			end
		end
		game.write_file("my.csv", table.concat(chartRow, ",").."\n", true)
	end
	game.write_file("my.csv", "\n", true)
	
	game.write_file("my.csv", "name,cov,sharp,maxprob,pla_den,r_base,r_mult,r_db,probpen,minamount,normamount\n", true)

	for name,chart in pairs(analysis.resourceChart) do
		local ep = game.entity_prototypes[name]
		local chartRow = {name}
		if ep then
			table.insert(chartRow, ep.autoplace_specification.coverage)
			table.insert(chartRow, ep.autoplace_specification.sharpness)
			table.insert(chartRow, ep.autoplace_specification.max_probability)
			table.insert(chartRow, ep.autoplace_specification.placement_density)
			table.insert(chartRow, ep.autoplace_specification.richness_base)
			table.insert(chartRow, ep.autoplace_specification.richness_multiplier)
			table.insert(chartRow, ep.autoplace_specification.richness_multiplier_distance_bonus)
			table.insert(chartRow, ep.autoplace_specification.random_probability_penalty)
			table.insert(chartRow, ep.minimum_resource_amount)
			table.insert(chartRow, ep.normal_resource_amount)
			if ep.infinite_resource then
				table.insert(chartRow, "true")
			else
				table.insert(chartRow, "false")
			end
		end
		game.write_file("my.csv", table.concat(chartRow, ",").."\n", true)
	end
	
	game.write_file("my.csv", "\n", true)
	
	collectgarbage("collect")
	wall("Recorded data for radius "..analysis.radius)
end


hexChars = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", }
function hexByte(num)
	return hexChars[math.floor(num/16)+1]..hexChars[num%16+1]
end

function startScan(radius)
	local foundScanLevel = false
	
	for _,scanlevel in ipairs(scanlevels) do
		if scanlevel.radius == radius then
			foundScanLevel = true
			if scanlevel.actual == scanlevel.expected then
				wall("Alread scanned radius "..radius)
				startAnalysis(game.surfaces[1], radius)
			else
				wall("Starting Scan for radius "..radius)
				scanlevel.record = true
				for __,force in pairs(game.forces) do
					force.chart(game.surfaces[1], {
						lefttop = {x = -radius, y = -radius}, 
						rightbottom = {x = radius, y = radius}})
				end
				game.speed = 10
			end
		end
	end
	
	if not foundScanLevel then
		wall("Scan radius "..radius.." not supported.")
	end
	
end

function reveal(radius)
	game.player.force.chart(game.player.character.surface, {
		lefttop = {x = -radius, y = -radius}, 
		rightbottom = {x = radius, y = radius}})
end

function endScan()
	game.speed = 1
end



script.on_init( function (e)

end
)
script.on_load( function (e)
	
end
)

script.on_event( {defines.events.on_tick}, function (e)

	if analysis.active then
		continueAnalysis()
	end

	if global.scanStarted then
		return
	end

	global.scanStarted = true
	
	if settings.global["dansmod-active"].value then
		startScan(settings.global["dansmod-scanradius"].value)
	end

end
)

script.on_event( {defines.events.on_chunk_generated}, function(e)
	local c = Chunk.areaToCenter(e.area)
	
	for _,scanlevel in ipairs(scanlevels) do
		if c.x < scanlevel.radius and c.y < scanlevel.radius 
				and c.x > -scanlevel.radius and c.y > -scanlevel.radius then
			scanlevel.actual = scanlevel.actual+1
			if scanlevel.actual>scanlevel.expected then
				if not wtf then
					wall( "Wtf? "..scanlevel.actual..">"..scanlevel.expected.." ("..c.x..","..c.y..")" )
					wtf = true
				end
			elseif scanlevel.actual==scanlevel.expected and scanlevel.record then
				startAnalysis(game.surfaces[1], scanlevel.radius)
				scanlevel.record = false
				endScan()
			end 
		end
	end
	
end
)

function easymode()
  game.player.cheat_mode=true
  game.player.force.research_all_technologies()
  game.player.insert{name="car"}
  game.player.insert{name="rocket-fuel", count=50}
  game.player.surface.peaceful_mode = true
end

character = nil

function godmode()
  local currentCharacter = game.player.character
  if currentCharacter then character=currentCharacter end
  game.player.character = nil
end

function godmodeOff()
  if character and not game.player.character then
    game.player.character = character
  end
end


function checkIfTableContainsKey(tbl, val)
	local present = false
	pcall(function()
		present = tbl[val] ~= nil
	end)
	return present
end

function listResources()
	
	local apValues = {
		"coverage",
		"max_probability",
		"placement_density",
		"richness_base",
		"richness_multiplier",
		"richness_multiplier_distance_bonus"
	}

	local values = {
		"minimum_resource_amount",
		"normal_resource_amount"
	}

	local headers = {"name","color"}
	for _,value in ipairs(values) do
		table.insert(headers, value)
	end
	for _,apValue in ipairs(apValues) do
		table.insert(headers, apValue)
	end
	
	local resourceList = {table.concat(headers,", ")}
	for name, ep in pairs(game.entity_prototypes) do
		if ep.resource_category then
			local thisResource = {name, 
				"#"..ep.map_color.r.."."..ep.map_color.g.."."..ep.map_color.b }
			for _,value in ipairs(values) do
				table.insert(thisResource, ep[value])
			end
			if ep.autoplace_specification then
				for _,apValue in ipairs(apValues) do
					table.insert(thisResource, ep.autoplace_specification[apValue])
				end
			else
				table.insert(thisResource, "no autoplace")
			end
			table.insert(resourceList, table.concat(thisResource,", "))
		end
	end
	local allData = table.concat(resourceList, "\n")
	game.write_file("dansresources.csv", allData, true)
	
end



commands.add_command("daneasy", "Turn on easy mode.", easymode)
commands.add_command("dangod", "Turn on god mode.", godmode)
commands.add_command("dannogod", "Turn off god mode.", godmodeOff)
commands.add_command("danscan512", "Start a scan.", function() startScan(512) end)
commands.add_command("danscan1024", "Start a scan.", function() startScan(1024) end)
commands.add_command("danscan2048", "Start a scan.", function() startScan(2048) end)
commands.add_command("danscan4096", "Start a scan.", function() startScan(4096) end)
commands.add_command("danreveal512", "Start a scan.", function() reveal(512) end)
commands.add_command("danreveal1024", "Start a scan.", function() reveal(1024) end)
commands.add_command("danreveal2048", "Start a scan.", function() reveal(2048) end)
commands.add_command("danreveal4096", "Start a scan.", function() reveal(4096) end)
commands.add_command("danalyze512", "Start a scan.", function() startAnalysis(game.surfaces[1], 512) end)
commands.add_command("danalyze1024", "Start a scan.", function() startAnalysis(game.surfaces[1], 1024) end)
commands.add_command("danalyze2048", "Start a scan.", function() startAnalysis(game.surfaces[1], 2048) end)
commands.add_command("danalyze4096", "Start a scan.", function() startAnalysis(game.surfaces[1], 4096) end)
commands.add_command("danres", "List resource types.", listResources)
commands.add_command("danseed", "Print the seed.", function() game.player.print("seed: "..game.surfaces[1].map_gen_settings.seed) end )


