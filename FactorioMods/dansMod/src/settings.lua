data:extend({
	{
		type = "bool-setting",
		name = "dansmod-active",
		setting_type = "runtime-global",
		default_value = false,
	},
	{
		type = "int-setting",
		name = "dansmod-scanradius",
		setting_type = "runtime-global",
		default_value = 1024,
	}

})


