require "test.Test"
require "danlib.ChunkTest"
require "pfr.TestDeposit"
require "img.XpmLibTest"

local function testAll()
	Test.testAll("ChunkTest",ChunkTest)
	Test.testAll("TestDeposit", TestDeposit)
	Test.testAll("XpmLibTest", XpmLibTest)
end

local function printDeposit()
	local d = Deposit.generateNew("copper",92,131,2000000)
	XpmLib.writeToFile(d:xpmData(),"deposit.xpm")
	print(d:toString())
end

testAll()
--printDeposit()

