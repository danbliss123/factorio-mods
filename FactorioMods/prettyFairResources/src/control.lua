-- TODO Add tool to produce manual customization data
-- TODO Add option to exclude resource from starting area
-- TODO Improve interpretation of autoplace specifications
-- TODO Bug: regen can create a thin band around the revealed area that contains no resources (it does contain resource islands in water, just nothing on them).

require "danlib.Chunk"
require "danlib.FactorioUtils"
require "pfr.MapManager"
require "pfr.Resources"
require "resourceCustomization"

logfile="pfr.txt"

function wall(message)
	local fullmessage = "(PFR) "..message
	for _,player in pairs(game.players) do
		player.print(fullmessage)
	end
end

function newRandomGenerator(seed)
	if seed<0 then
		return game.create_random_generator()
	else
		return game.create_random_generator(seed)
	end
end

RemotelySpecifiedResources = {}

function addResources( modName, resources )
	RemotelySpecifiedResources[modName] = resources
end

function getPfrResourcesFromOtherMods()
	for remoteName, remoteInterface in pairs(remote.interfaces) do
		if remoteInterface["get-pfr-resources"] then
			addResources(
				remoteName,
				remote.call(remoteName, "get-pfr-resources")
			)
		end
	end
end


function pfrTextOut(message)
	game.write_file(logfile, message, true)
end

function putChunkOnMap(surface, chunk)
	local topLeft = Chunk.toTopLeft(chunk)
	local createdResources = 0

	local waterToDestroy = {}

	for y=1,Chunk.chunksize,1 do
		local sy = topLeft.y+y-.5
		for x=1,Chunk.chunksize,1 do
			local sx = topLeft.x+x-.5
			if chunk.values[y][x]>=0 then
				local existingTile = surface.get_tile{x=sx, y=sy}
				if existingTile.collides_with("water-tile") then
					table.insert( waterToDestroy, {name="sand-1", position={x=math.floor(sx), y=math.floor(sy)}} )
				end
			end
		end
	end
	if #waterToDestroy > 0 then
		surface.set_tiles(waterToDestroy)
	end
	for y=1,Chunk.chunksize,1 do
		local sy = topLeft.y+y-.5
		for x=1,Chunk.chunksize,1 do
			local sx = topLeft.x+x-.5
			if chunk.values[y][x]>0 then
				surface.create_entity{name=chunk.resource, initial_amount=chunk.values[y][x], amount=chunk.values[y][x], position={x=sx,y=sy}}
			end
		end
	end
end

function processChunk(area, surface)
	if not global.pfrmodActive then
		return
	end

	local surfaceInfo = global.surfaces[surface.name]
	
	if not surfaceInfo then
		addSurface(surface.index)
		surfaceInfo = global.surfaces[surface.name]
	end
	
	if surfaceInfo.active then

		-- Remove resources on the chunk.
		local resourcesToDestroy = surface.find_entities_filtered{area=area, type='resource'}
		for i,r in ipairs(resourcesToDestroy) do
			r.destroy()
		end
		
		local chunk = Chunk.fromArea(area)
		
		local cell = Chunk.toCell(chunk)
		
		MapManager.generateUpTo(surfaceInfo.map,surfaceInfo.mapRandom,cell.ring)
		
		local cellinfo = surfaceInfo.map.rings[cell.ring].cellinfos[cell.cellnum]
		
		if cellinfo.status==MapManager.Status.UNGENERATED then
			MapManager.generateCell(game.create_random_generator(cellinfo.seed),cell,surfaceInfo.map.rings[cell.ring].cellinfos[cell.cellnum],surfaceInfo.resources)
		end
		
		local chunkId = Chunk.toIdString(chunk)
		
		if cellinfo.status == MapManager.Status.GENERATED and cellinfo.chunks[chunkId] then
			local chunkList = cellinfo.chunks[chunkId]
			cellinfo.chunks[chunkId] = nil
			for _,thisChunk in ipairs(chunkList) do
				putChunkOnMap(surface, thisChunk)
			end
			local hasMoreChunks = false
			for _,remainingChunkList in pairs(cellinfo.chunks) do
				if remainingChunkList and #remainingChunkList>0 then
					hasMoreChunks=true
					break
				end
			end
			if not hasMoreChunks then
				cellinfo.chunks = {}
				cellinfo.status = MapManager.Status.FINISHED
			end
		end
	end
end

function regen(reroll, silent)
	if not silent then
		if global.pfrmodActive then
			if reroll then
				game.player.print("Re-activating PFR mod with new random seed.")
				global.statusText=global.statusText.."  PFR regened (random seed)."
			else
				game.player.print("Re-activating PFR mod with existing seed.")
				global.statusText=global.statusText.."  PFR regened (map seed)."
			end
		else
			if reroll then
				game.player.print("Activating PFR mod with new random seed.")
				global.statusText=global.statusText.."  PFR activated (random seed)."
			else
				game.player.print("Activating PFR mod with existing seed.")
				global.statusText=global.statusText.."  PFR activated (map seed)."
			end
		end
	end
	if reroll then
		global.seed = math.random(2147483647)
		global.seeds={}
	end
	global.random = newRandomGenerator(global.seed)
	
	global.pfrmodActive=true
	global.surfaces = {}
	
	local i=0
	for _,surface in pairs(game.surfaces) do
		for chunk in surface.get_chunks() do
			processChunk(Chunk.toArea(chunk), surface)
			i=i+1
			if i>=100 then
				collectgarbage("collect")
				i=0
			end
		end
	end
	collectgarbage("collect")
	
end

function addSurface(index)
	local surface = game.surfaces[index]
	local surfaceInfo = {}
	surfaceInfo.active = true
	if not global.seeds[surface.name] then
		global.seeds[surface.name] = global.random(2147483647)
	end
	surfaceInfo.mapRandom = newRandomGenerator(global.seeds[surface.name])
	surfaceInfo.name = surface.name
	surfaceInfo.resources = ResourceUtils.generateResourceSet(surface, RemotelySpecifiedResources)
	surfaceInfo.map = MapManager.new(surfaceInfo.resources)
	MapManager.generateUpTo(surfaceInfo.map,surfaceInfo.mapRandom,2)
	global.surfaces[surface.name] = surfaceInfo
end

script.on_init ( function()
	RemotelySpecifiedResources = {}
	getPfrResourcesFromOtherMods()
	addResources("manual-preset", PresetResources)
	global.seed = -1
	global.random = newRandomGenerator(global.seed)
	global.version=2
	global.surfaces = {}
	global.seeds = {}
	if game.tick>0 then
		global.pfrmodActive=false
		global.statusText="PFR mod inactive because game was generated before PFR mod was installed."
	elseif not settings.global["pfrmod-active"].value then
		global.pfrmodActive=false
		global.statusText="PFR mod inactive because it was turned off when this game was generated."
	else
		global.pfrmodActive=true
		global.statusText="PFR mod active."
	end
end
)

script.on_load ( function()
	RemotelySpecifiedResources = {}
	getPfrResourcesFromOtherMods()
	addResources("manual-preset", PresetResources)
end
)

script.on_configuration_changed ( function() 
	if global.version==1 then
		global.surfaces={}
		global.seeds={}
		
		for name,surface in pairs(game.surfaces) do
			global.surfaces[name] = {active=false}
		end
		
		global.surfaces["nauvis"] = {}
		
		global.surfaces["nauvis"].active = true
		global.seeds["nauvis"] = -1
		global.surfaces["nauvis"].mapRandom = global.mapRandom
		global.mapRandom = nil
		global.seed = -1
		global.random = newRandomGenerator(global.seed)
		global.surfaces["nauvis"].name = "nauvis"
		global.surfaces["nauvis"].resources = global.resources
		global.resources = nil
		global.surfaces["nauvis"].map = global.map
		global.map = nil
		
		global.version=2
	end
end)

script.on_event( {defines.events.on_chunk_generated}, function(e)
	if global.pfrmodActive then
		processChunk(e.area, e.surface)
	end
end
)



commands.add_command("pfr_active", "Check if PFR is active", function()
	if global.pfrmodActive then
		game.player.print( "PFR active.")
	else
		game.player.print( "PFR inactive.")
	end
	game.player.print(global.statusText)
end)

commands.add_command("pfr_log_map_cheats", "Write cheaty information about resource locations to "..logfile, function()
	for _,surfaceInfo in global.surfaces do
		pfrTextOut("Surface: "..surfaceInfo.name)
		pfrTextOut(MapManager.toString(surfaceInfo.map))
		pfrTextOut("")
		game.player.print("Wrote patch location information to "..logfile.." in the scriptOutput directory. For help understanding the information, see patchLocationHelp.txt in the prettyFairResources mod zip/directory.")
	end
end)

commands.add_command("pfr_resource_info", "Print resource info", function()
	local surface = game.player.character.surface
	game.player.print("Resource in surface "..surface.name)
	for name,info in pairs(global.surfaces[surface.name].resources) do
		game.player.print(name..": "..(info.baseDensity or "x")..", "..(info.frequency or "x"))
	end
end)

commands.add_command("pfr_regen", "Regenerate all resources on the map", function()
	regen(false)
end)

commands.add_command("pfr_regen_with_new_seed", "Regenerate all resources on the map.  Use a new random seed.", function()
	regen(true)
end)






