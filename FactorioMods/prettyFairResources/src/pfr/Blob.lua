
SimpleBlob = {
	a=1,
	b=1,
	c=0,
	x=0,
	y=0,
	z=1,
	minx=-1,
	maxx=1,
	miny=-1,
	maxy=1
}

function SimpleBlob.fromParameters(a, b, th, x, y, z)
	local cos = math.cos(th)
	local sin = math.sin(th)
	local c2 = cos*cos
	local s2 = sin*sin
	local cs = cos*sin
	
	local a2 = a*a
	local b2 = b*b
	
	local factor = 1/(a2*b2)
	
	local bigDimension = a
	if b>a then bigDimension = b end 
	
	return {
		a = (b2*c2+a2*s2)*factor,
		b = (b2*s2+a2*c2)*factor,
		c = 2*cs*(a2-b2)*factor,
		x=x, y=y, z=z,
		minx=x-bigDimension,
		maxx=x+bigDimension,
		miny=y-bigDimension,
		maxy=y+bigDimension,
	}
end

function SimpleBlob.evaluate(blob, x, y)
	x = x-blob.x
	y = y-blob.y

	local fvalue = (1 - blob.a*x*x - blob.b*y*y - blob.c*x*y)
	
	if fvalue < 0 then return 0 end
	
	return fvalue * fvalue * fvalue * fvalue * blob.z
end

function SimpleBlob.toString(blob)
	return "Blob = "..blob.z.."*(1 - "..blob.a.."*(x-"..blob.x..")^2 + "..blob.b.."*(y-"..blob.y..")^2 + "..blob.c.."*(x-..)(y-..) )^2  ["..blob.minx.."<x<"..blob.maxx..", "..blob.miny.."<y<"..blob.maxy.."]"
end

ComplexBlob = {
	blobs = {},
	threshold = .5,
	scale = 1,
	minx=-1,
	maxx=1,
	miny=-1,
	maxy=1
}

function ComplexBlob.evaluate(cBlob, x, y)
	local truex = cBlob.minx+x*(cBlob.maxx-cBlob.minx)
	local truey = cBlob.miny+y*(cBlob.maxy-cBlob.miny)

	local total = -cBlob.threshold;
	for _,blob in ipairs(cBlob.blobs) do
		local thisValue = SimpleBlob.evaluate(blob, truex, truey)
		total = total+thisValue
	end
	return total
end

function ComplexBlob.create(blobs)
	local cBlob = {blobs=blobs}
	local minx=blobs[1].x
	local maxx=blobs[1].x
	local miny=blobs[1].y
	local maxy=blobs[1].y
	
	for _,blob in ipairs(blobs) do
		if blob.minx < minx then minx = blob.minx end
		if blob.maxx > maxx then maxx = blob.maxx end
		if blob.miny < miny then miny = blob.miny end
		if blob.maxy > maxy then maxy = blob.maxy end
	end
	
	return {
		blobs=blobs,
		minx=minx,
		maxx=maxx,
		miny=miny,
		maxy=maxy,
		threshold=0,
	}
	
end

function ComplexBlob.toString(cBlob) 
	local strings = {}
	for _,blob in ipairs(cBlob.blobs) do
		table.insert(strings, SimpleBlob.toString(blob))
	end
	table.insert(strings, "threshold="..cBlob.threshold.."["..cBlob.minx.."<x<"..cBlob.maxx..", "..cBlob.miny.."<y<"..cBlob.maxy.."]")
	return table.concat(strings,"\n")
end

-- cBlob the blob to draw
-- raster the raster to draw to
-- top the first row of the raster to be part of the blob
-- left the first column of the raster to be part of the blob
-- height the height of the draw area on the raster
-- width the width of the draw area on the raster
-- value the value to draw to the raster if blob is present
function ComplexBlob.draw(cBlob, raster, top, left, height, width, value, bordervalue, inversevalue)
	
	local yfactor = 1/height
	local xfactor = 1/width
	
	for y = 1,height,1 do
		local py = y+top-1
		local by = (y-.5)*yfactor
		for x = 1,width,1 do
			local px = x+left-1
			local bx = (x-.5)*xfactor
			local evaluation = ComplexBlob.evaluate(cBlob,bx,by)
			if evaluation>0 then
				raster[py][px] = value
			elseif evaluation==0 and bordervalue then
				raster[py][px] = bordervalue
			elseif inversevalue then
				raster[py][px] = inversevalue
			end
		end
	end
	
end

function ComplexBlob.random3(random)
	local r = 1-2*random()
	return (r*r*r+r)/2
end

ComplexBlob.ComplexBlobProfile = {
	count=1,
	longAxisMin=1,
	longAxisMax=1,
	aspectRatioMin=1,
	aspectRatioMax=2,
	placementRadius=2,
	zMin=.4,
	zMax=.8
}

ComplexBlob.defaultProfiles = {
	{
		count=1,
		longAxisMin=3, longAxisMax=3,
		aspectRatioMin=1, aspectRatioMax=1,
		placementRadius=0,
		zMin=1.1, zMax=1.1
	},
	{
		count=8,
		longAxisMin=2, longAxisMax=3,
		aspectRatioMin=1, aspectRatioMax=2,
		placementRadius=3,
		zMin=.2, zMax=.3
	},
--	{
--		count=6,
--		longAxisMin=.5, longAxisMax=1.5,
--		aspectRatioMin=1, aspectRatioMax=2,
--		placementRadius=2,
--		zMin=-.3, zMax=-.1
--	},
	{
		count=8,
		longAxisMin=.5, longAxisMax=1.5,
		aspectRatioMin=1, aspectRatioMax=2,
		placementRadius=2,
		zMin=.1, zMax=.2
	},
}

function ComplexBlob.generate(random, profiles)
	local blobs = {}
	
	for _,profile in ipairs(profiles) do
		local zTotalExpected = (profile.count-.5)*(profile.zMax+profile.zMin)/2
		local zTotal=0
		while zTotal<zTotalExpected do
			local longAxis = profile.longAxisMin+random()*(profile.longAxisMax-profile.longAxisMin)
			local shortAxis = longAxis / (profile.aspectRatioMin+random()*(profile.aspectRatioMax-profile.aspectRatioMin))
			local z = profile.zMin+random()*(profile.zMax-profile.zMin)
			zTotal = zTotal+z
			table.insert(blobs, SimpleBlob.fromParameters(
			longAxis,shortAxis,
			random()*math.pi,
			profile.placementRadius*ComplexBlob.random3(random),profile.placementRadius*ComplexBlob.random3(random),
			z))
		end
	end
	
--	table.insert(blobs, SimpleBlob.fromParameters(
--		2,2,
--		0,
--		0,0,.3))
--
--	for i = 1,3,1 do
--		table.insert(blobs, SimpleBlob.fromParameters(
--			1+2*random(),1+2*random(),
--			random()*math.pi,
--			1*ComplexBlob.random3(random),1*ComplexBlob.random3(random),.3+.4*random()))
--	end
--	
--	for i = 1,4,1 do
--		table.insert(blobs, SimpleBlob.fromParameters(
--			.5+1*random(),.5+1*random(),
--			random()*math.pi,
--			2*ComplexBlob.random3(random),2*ComplexBlob.random3(random),-.3+.1*random()))
--	end
--	
--	for i = 1,3,1 do
--		table.insert(blobs, SimpleBlob.fromParameters(
--			.5+.5*random(),.5+.5*random(),
--			random()*math.pi,
--			1.5*ComplexBlob.random3(random),1.5*ComplexBlob.random3(random),.4+.1*random()))
--	end
	
	local cBlob = ComplexBlob.create(blobs,0)
	
	local samples = {}
	
	for y=1,9,2 do
		for x=1,9,2 do
			table.insert(samples,ComplexBlob.evaluate(cBlob,x/10,y/10))
		end
	end
	
	table.sort(samples)
	
	--cBlob.threshold=samples[20]
	cBlob.threshold=.2
	
	return cBlob
	
end






