
require "pfr.Blob"

Deposit = {
	resource = "",
	height = 0,
	width = 0,
	amount = 0,
	values = {},
}

Deposit.__index = Deposit

function Deposit.new(obj)
	obj = obj or {}
	setmetatable(obj, Deposit)
	return obj
end

function Deposit.generateNew(random, resource, oneMilPatchWidth, amount)

	local border=5;

	local rootAmount = math.pow(amount,1/3)/45*oneMilPatchWidth
	local height = math.ceil(rootAmount)+border*2
	local width = math.ceil(rootAmount)+border*2
	
	local bigValues = {}
	
	bigValues[1] = {}
	
	local cBlob = ComplexBlob.generate(random,ComplexBlob.defaultProfiles)
	
	local minx = width
	local maxx = 1
	local miny = height
	local maxy = 1
	
	local total = 0;
	
	-- create the initial deposit
	for y=1,height,1 do
		local by = (y-.5-border)/(height-border*2)
		local row = {}
		for x=1,width,1 do
			local bx = (x-.5-border)/(width-border*2)
			local value = ComplexBlob.evaluate(cBlob,bx,by)
			if value>0 then
				if x-1<minx then minx=x-1 end
				if x+1>maxx then maxx=x+1 end
				if y-1<miny then miny=y-1 end
				if y+1>maxy then maxy=y+1 end
				row[x] = value / (value+.45)
				total = total+value
			else
				row[x] = -1
			end
		end
		bigValues[y] = row
	end
	
	local scale = amount/total
	local expectedAmountSoFar = 0
	local actualAmountSoFar = 0
	
	local finalHeight = maxy-miny+1
	local finalWidth = maxx-minx+1
	
	local finalValues = {}
	for y=1,finalHeight,1 do
		local finalRow = {}
		local previousBigRow = bigValues[y+miny-2]
		local nextBigRow = bigValues[y+miny]
		local bigRow = bigValues[y+miny-1]
		for x=1,finalWidth,1 do
			if bigRow[x+minx-1]>0 then
				expectedAmountSoFar = expectedAmountSoFar + bigRow[x+minx-1] * scale
				local nextActualTotal = math.floor(expectedAmountSoFar+.5)
				finalRow[x] = nextActualTotal - actualAmountSoFar
				actualAmountSoFar = nextActualTotal
			elseif bigRow[x+minx-2]>0 or bigRow[x+minx]>0 then
				finalRow[x] = 0
			elseif previousBigRow and ( previousBigRow[x+minx-2]>0 or previousBigRow[x+minx-1]>0 or previousBigRow[x+minx]>0 ) then
				finalRow[x] = 0
			elseif nextBigRow and ( nextBigRow[x+minx-2]>0 or nextBigRow[x+minx-1]>0 or nextBigRow[x+minx]>0 ) then
				finalRow[x] = 0
			else
				finalRow[x] = -1
			end
		end
		finalValues[y] = finalRow
	end
	
	return Deposit.new({resource=resource, height=finalHeight, width=finalWidth, amount=amount, values=finalValues})
end

function Deposit.generateNewFluid(random, resource, averageAmount, amount)

	local border = 5

	averageAmount = math.ceil(averageAmount)
	local desiredTotalWells = math.ceil(amount/averageAmount);

	local noBorderWidth = math.ceil(math.sqrt(desiredTotalWells*40))
	local height = noBorderWidth+border*2
	local width = height
	
	local values = {}
	
	-- create blank canvas
	for y=1,height,1 do
		local row = {}
		for x=1,width,1 do
			row[x] = -1
		end
		values[y] = row
	end
	
	local minx = width
	local maxx = 1
	local miny = height
	local maxy = 1
	
	local totalWells = 0
	local totalValue = 0
	
	local distanceLimitSquared = (width*width)/4
	
	while totalWells<desiredTotalWells do
		local y = border+random(noBorderWidth)
		local x = border+random(noBorderWidth)
		if (x-width/2)*(x-width/2)+(y-height/2)*(y-height/2) < distanceLimitSquared and values[y][x]==-1 then
			for by = y-2,y+2,1 do
				for bx = x-2,x+2,1 do
					values[by][bx] = 0
				end
			end
			if x-2<minx then minx=x-2 end
			if x+2>maxx then maxx=x+2 end
			if y-2<miny then miny=y-2 end
			if y+2>maxy then maxy=y+2 end
			values[y][x]=averageAmount/2+random(averageAmount)
			totalWells = totalWells+1
			totalValue = totalValue+values[y][x]
		end
	end
	
	local scale = amount/totalValue
	
	local expectedAmountSoFar = 0
	local actualAmountSoFar = 0

	
	local finalHeight = maxy-miny+1
	local finalWidth = maxx-minx+1
	
	local finalValues = {}
	for y=1,finalHeight,1 do
		local finalRow = {}
		local bigRow = values[y+miny-1]
		for x=1,finalWidth,1 do
			if bigRow[x+minx-1]>0 then
				expectedAmountSoFar = expectedAmountSoFar + bigRow[x+minx-1] * scale
				local nextActualTotal = math.floor(expectedAmountSoFar+.5)
				finalRow[x] = nextActualTotal - actualAmountSoFar
				actualAmountSoFar = nextActualTotal
			elseif bigRow[x+minx-1]==0 then
				finalRow[x] = 0
			else
				finalRow[x] = -1
			end
		end
		finalValues[y] = finalRow
	end
	
	return Deposit.new({resource=resource, height=finalHeight, width=finalWidth, amount=amount, values=finalValues})
end

function Deposit:selfTest()
	local failures = {}
	local total = 0
	for y = 1,self.height,1 do
		for x = 1,self.width,1 do
			if self.values[y][x] >= 0 then
				total = total + self.values[y][x]
			elseif self.values[y][x] ~= -1 then
				table.insert(failures,"Illegal value at position "..y..","..x..": "..self.values[y][x])
			end
		end
		if self.values[y][0] then
			table.insert(failures,"Zeroith element on line "..y)
		end
		if self.values[y][self.width+1] then
			table.insert(failures,"Extra element on line "..y)
		end
	end
	if self.values[0] then
		table.insert(failures,"Zeroith row")
	end
	if self.values[self.height+1] then
		table.insert(failures,"Extra row")
	end
	if total ~= self.amount then
		table.insert(failures,"Amount mismatch. Expected "..self.amount..", found "..total)
	end
	return failures
end

function Deposit:toString()
	local lines = {"Deposit of "..self.amount.." "..self.resource.." ("..self.height.."x"..self.width..")\n"}
	local total = 0
	for y = 1,self.height,1 do
		local line = {}
		for x = 1,self.width,1 do
			table.insert(line, Deposit.charForNumber(self.values[y][x]).." ")
			total = total + self.values[y][x]
		end
		table.insert(lines,table.concat(line).."\n")
	end
	return table.concat(lines)..table.concat(self:selfTest(),"\n")
end

-- returns an array of tables with {x=chunkx, y=chunky, resource=name, []=array of rows}
-- topLeftLocation is {x=,y=}.  0,0 indicates it should be just barely in the fourth quadrant
function Deposit:chunkify(topLeftLocation)
	local chunkLeft = math.floor(topLeftLocation.x/Chunk.chunksize)
	local chunkRight = math.ceil((topLeftLocation.x+self.width)/Chunk.chunksize-1)
	local chunkTop = math.floor(topLeftLocation.y/Chunk.chunksize)
	local chunkBottom = math.ceil((topLeftLocation.y+self.height)/Chunk.chunksize-1)
	
	local chunkArray = {}
	
	-- print( "Deposit "..self.resource.." "..self.height.."x"..self.width.." tl="..topLeftLocation.y..","..topLeftLocation.x.." "..chunkTop..","..chunkLeft )
	
	for cr = chunkTop,chunkBottom,1 do
		for cc = chunkLeft,chunkRight,1 do
			local chunk = {x=cc, y=cr, resource=self.resource, values={}}
			local chunkHasContent = false
			local lrOffset = cr*Chunk.chunksize-topLeftLocation.y
			local lcOffset = cc*Chunk.chunksize-topLeftLocation.x
			-- print ("  Chunk "..cr..","..cc..","..lrOffset..","..lcOffset)
			for lr=1,Chunk.chunksize,1 do
				local dr = lrOffset+lr
				chunk.values[lr] = {}
				for lc=1,Chunk.chunksize,1 do
					local dc = lcOffset+lc
					if dc>0 and dc<=self.width and dr>0 and dr<=self.height then
						local value = self.values[dr][dc]
						if value>=0 then
							chunk.values[lr][lc] = self.values[dr][dc]
							chunkHasContent = true
						else
							chunk.values[lr][lc] = -1
						end
					else
						chunk.values[lr][lc] = -1
					end
				end
			end
			if chunkHasContent then
				table.insert(chunkArray, chunk)
			end
		end
	end
		
	return chunkArray
end

function Deposit.charForNumber(num)
	if num< -1 then
		return "?"
	elseif num<0 then
		return " "
	elseif num==0 then
		return "-"
	elseif num<100 then
		return "*"
	elseif num<200 then
		return "1"
	elseif num<500 then
		return "2"
	elseif num<1000 then
		return "3"
	elseif num<2000 then
		return "4"
	elseif num<5000 then
		return "5"
	elseif num<10000 then
		return "6"
	elseif num<20000 then
		return "7"
	elseif num<50000 then
		return "8"
	elseif num<100000 then
		return "9"
	else
		return "X"
	end
end

function Deposit.intForNumber(num)
	if num< -1 then
		return 14
	elseif num<0 then
		return 1
	elseif num==0 then
		return 2
	elseif num<100 then
		return 3
	elseif num<200 then
		return 4
	elseif num<500 then
		return 5
	elseif num<1000 then
		return 6
	elseif num<2000 then
		return 7
	elseif num<5000 then
		return 8
	elseif num<10000 then
		return 9
	elseif num<20000 then
		return 10
	elseif num<50000 then
		return 11
	elseif num<100000 then
		return 12
	else
		return 13
	end
end

Deposit.colors = {
	"000000",
	"101010",
	"402010",
	"502810",
	"603010",
	"703810",
	"804010",
	"904810",
	"a05010",
	"b05810",
	"c06010",
	"d06810",
	"e07010",
	"f07810",
}

function Deposit:xpmData()
	local raster = {}
	for r,row in ipairs(self.values) do
		local pixelRow = {}
		for c,tile in ipairs(row) do
			table.insert(pixelRow, Deposit.intForNumber(tile))
		end
		table.insert(raster, pixelRow)
	end
	return {colors=Deposit.colors, raster=raster}
end

