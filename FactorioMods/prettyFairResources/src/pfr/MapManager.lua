
require "danlib.Chunk"
require "pfr.Deposit"
require "resourceCustomization"

-- Cell info is {
--   patches = list of {resource = name, amount = amount}
--   chunks = dictionary of chunk ids to unhandled chunks
--   status = MapManager.Status value
-- }
-- Ring layer is {cellinfos = array of cellinfos}
-- Map manager is {rings = array of ring layers.}


MapManager = {rings={}, resources={}}

MapManager.Status = {
	UNGENERATED = 0,
	GENERATED = 1,
	FINISHED = 2
}

MapManager.Location = {
	NEAR = 0,
	FAR = 1,
	CENTER = 2,
	NOT_CENTER = 3
}

Ring = {cellinfos={}}

CellInfo = {patches={}, chunks={}, status=MapManager.Status.UNGENERATED}

Patch = {resource="", amount=0}

WrappedRandom = {
	__call = function(t, a, b) 
		if a==nil and b==nil then
			return t.wrappedRandom()
		elseif b==nil then
			local result = math.ceil(t.wrappedRandom()*a)
			if (result<1 or result>a) then
				return 1
			else
				return result
			end
		else
			local result = math.floor( a+t.wrappedRandom()*(a-b+1) )
			if (result<a or result>b) then
				return a
			else
				return result
			end
		end
	end
}
WrappedRandom.__index = WrappedRandom

function MapManager.normalizeRandom(random)
	local newRandom = {
		wrappedRandom = random
	}
	return setmetatable(newRandom,WrappedRandom)
end

function MapManager.distanceFunction(distanceTiles)
	-- hyperbola with asymptote y=x/MapGenerationTuning.bonusDistance, but going to 1 at x=0
	local linearBonus = distanceTiles/MapGenerationTuning.bonusDistance
	local bonus = math.sqrt(1+linearBonus*linearBonus)
	return bonus
end

MapGenerationTuning.patchMinSize = 2/(MapGenerationTuning.patchSizeMaxRatio+1)
MapGenerationTuning.patchSizeRange = 2-2*MapGenerationTuning.patchMinSize

function MapManager.new(resources)
	return {rings={}, resources=resources}
end

-- if mapManager is nil, this creates and returns a new mapManager
function MapManager.generateUpTo(mapManager, unsafeRandom, maxRing, resources)
	local random = MapManager.normalizeRandom(unsafeRandom)

	if not mapManager then
		mapManager = MapManager.new(resources)
	end
	if not mapManager.rings[0] then
		mapManager.rings[0] = {
			cellinfos = MapManager.generateStartingArea(random, mapManager.resources)
		}
	end
	for i = 1,maxRing,1 do
		if not mapManager.rings[i] then
			mapManager.rings[i] = { cellinfos = MapManager.generateRing(random, i, mapManager.resources) }
		end
	end
	
	return mapManager
end

-- each resource is dictionary of name to {baseDensity=, frequency=}.  Ring is number of ring.
-- returns a ring layer to be used in a MapManager
function MapManager.generateRing(unsafeRandom, ring, resources)
	local random = MapManager.normalizeRandom(unsafeRandom)
	
	--local cellWidth = Cell.widthChunks({ring=ring, cellnum=1})
	--local quantityFactor = cellWidth*cellWidth*12*MapGenerationTuning.quantityFunction(Cell.distanceFromOrigin({ring=ring, cellnum=1}))
	
	local cells = {}
	
	for c = 1,Cell.count,1 do
		cells[c] = {
			patches = {},
			chunks = {},
			seed = random(65535),
			status = MapManager.Status.UNGENERATED
		}
	end
	
	local expectedTotalPatches = 0
	for resource, rinfo in pairs(resources) do
		expectedTotalPatches = expectedTotalPatches+rinfo.frequency
	end
	local patchLimit = expectedTotalPatches/12
	
	local desperation = 0
	
	for resource, rinfo in pairs(resources) do
		local near = true
		local patchSize = 1/(rinfo.frequency*MapGenerationTuning.defaultPatchCount)
		local patches = {}
		local total = 0
		while total<1 do
			local value = (MapGenerationTuning.patchMinSize+random()*MapGenerationTuning.patchSizeRange)*patchSize
			table.insert(patches, value)
			total = total+value
		end
		local scale = 1/total
		local maxPatchCount = #patches / Cell.count
		local patchCounts = {}
		for i=1,Cell.count,1 do
			patchCounts[i] = 0
		end
		for p, patchAmount in ipairs(patches) do
			local location
			if near then location=MapManager.Location.NEAR else location=MapManager.Location.FAR end
			near = not near
			while true do
				local cellId=-1
				local cellIdDesperation = -.3
				
				while cellId<0 or patchCounts[cellId]>=maxPatchCount+cellIdDesperation do
					cellId = MapGenerationTuning.cellIdTable[random(#MapGenerationTuning.cellIdTable)]
					cellIdDesperation = cellIdDesperation+.1
				end
				if cellIdDesperation>1 then
					log("Reached cell ID desperation "..cellIdDesperation.." while generating "..resource.." in ring "..ring)
				end
				
				if #cells[cellId].patches <= patchLimit+desperation then
					table.insert( cells[cellId].patches, {resource=resource,amount=patchAmount*scale, location=location})
					patchCounts[cellId] = patchCounts[cellId]+1
					break
				else
					desperation = desperation+MapGenerationTuning.ringDesperationFactor
				end
			end
		end
	end
	
	if desperation > 2 then
		log("Reached global cell ID desperation "..desperation.." in ring "..ring)
	end
	
	return cells

end

-- each resource is dictionary of name to {baseDensity=, frequency=}.  Ring is number of ring.
-- returns a ring layer to be used in a MapManager
function MapManager.generateStartingArea(unsafeRandom, resources)
	local random = MapManager.normalizeRandom(unsafeRandom)
	
	--local quantityFactor = Cell.sarc*Cell.sarc*4*MapGenerationTuning.quantityFunction(0)
	
	local cells = {
		{
			patches = {},
			chunks = {},
			seed = random(65535),
			status = MapManager.Status.UNGENERATED
		}
	}
	
	for resource, rinfo in pairs(resources) do
		local patchSize = 1/(rinfo.frequency*MapGenerationTuning.defaultPatchCount)
		local patches = {}
		local total = 0
		local firstPatch = true
		while total<.4 do
			local value = (MapGenerationTuning.patchMinSize+random()*MapGenerationTuning.patchSizeRange)*patchSize
			if firstPatch then value = value/2 end
			table.insert(patches, value)
			total = total+value
			firstPatch = false
		end
		local scale = .5/total
		local location = MapManager.Location.CENTER
		
		for p, patchAmount in ipairs(patches) do
			table.insert( cells[1].patches, {resource=resource, amount=patchAmount*scale, location=location})
			location = MapManager.Location.NOT_CENTER
		end
	end
	
	return cells

end


function MapManager.generateCell(unsafeRandom, cell, cellinfo, resources) 
	local random = MapManager.normalizeRandom(unsafeRandom)
	if cellinfo.status ~= MapManager.Status.UNGENERATED then
		return
	end

	local width = Cell.width(cell)
	local topLeft = Cell.toTopLeft(cell)
	
	-- The random values in the 0-1x0-1 cell will be adjusted based on location NEAR, FAR, CENTER or NOT_CENTER
	-- If diagonal, they will be on the origin side of y+x=1 for NEAR
	-- If not diagonal, they will be y<.5 for NEAR
	local leftRotates = 0
	local diagonal = false
	
	if cell.cellnum==1 then
		leftRotates=2; diagonal=true
	elseif cell.cellnum==4 then
		leftRotates=1; diagonal=true
	elseif cell.cellnum==7 then
		leftRotates=0; diagonal=true
	elseif cell.cellnum==10 then
		leftRotates=3; diagonal=true
	elseif cell.cellnum==2 or cell.cellnum==3 then
		leftRotates=2
	elseif cell.cellnum==5 or cell.cellnum==6 then
		leftRotates=1
	elseif cell.cellnum==8 or cell.cellnum==9 then
		leftRotates=0
	elseif cell.cellnum==11 or cell.cellnum==12 then
		leftRotates=3
	end
	
	local desperation = 0
	local desperationRelaxRestrictionsLimit = #cellinfo.patches * 2 + 1
	local desperationGiveUpLimit = #cellinfo.patches * 4 + 1
	local gaveUp = false
	
	for _,patch in ipairs(cellinfo.patches) do
		while true do
			local relativePositionY = random()
			local relativePositionX = random()
			
			if desperation<desperationRelaxRestrictionsLimit then
				-- Adjust the patch to accommodate Location
				if patch.location==MapManager.Location.NEAR then
					if diagonal and relativePositionX+relativePositionY>1 then
						relativePositionX = 1-relativePositionX
						relativePositionY = 1-relativePositionY
					elseif not diagonal and relativePositionY>.5 then
						relativePositionY = 1-relativePositionY
					end
				elseif patch.location==MapManager.Location.FAR then
					if diagonal and relativePositionX+relativePositionY<1 then
						relativePositionX = 1-relativePositionX
						relativePositionY = 1-relativePositionY
					elseif not diagonal and relativePositionY<.5 then
						relativePositionY = 1-relativePositionY
					end
				elseif patch.location==MapManager.Location.CENTER then
					relativePositionX = (relativePositionX-.5)*MapGenerationTuning.centerAreaWidth+.5
					relativePositionY = (relativePositionY-.5)*MapGenerationTuning.centerAreaWidth+.5
				else
					local inverseCenterAreaWidth = 1/MapGenerationTuning.centerAreaWidth
					local offsetrpx = (relativePositionX-.5)*2
					local offsetrpy = (relativePositionY-.5)*2
					while math.abs(offsetrpx) < MapGenerationTuning.centerAreaWidth and math.abs(offsetrpy) < MapGenerationTuning.centerAreaWidth do
						offsetrpx = offsetrpx*inverseCenterAreaWidth
					end
					relativePositionX = offsetrpx/2+.5
					relativePositionY = offsetrpy/2+.5
				end
				for i = 1,leftRotates,1 do
					local newRelativePositionX = relativePositionY
					relativePositionY = 1-relativePositionX
					relativePositionX = newRelativePositionX
				end
			end
			
			-- approx refers to the fact that the patches are fit within cells.
			-- The approx location is always within the bounding rectangle of the patch.
			local approxY = topLeft.y+relativePositionY*width
			local approxX = topLeft.x+relativePositionX*width
			local approxDSquared = approxX*approxX+approxY*approxY
			local distanceFactor = 1
			--local distanceFactor = MapManager.distanceFunction(math.sqrt(approxDSquared))
			
			local sizeFactor = (approxDSquared/1024)*12
			local startingAreaBonusFactor = resources[patch.resource].startingAreaBonus
			if not startingAreaBonusFactor then startingAreaBonusFactor=1 end
			if sizeFactor<MapGenerationTuning.minSizeFactor*startingAreaBonusFactor then sizeFactor=MapGenerationTuning.minSizeFactor*startingAreaBonusFactor end
		
			local deposit
			local thisQuantity = sizeFactor * distanceFactor * resources[patch.resource].baseDensity * patch.amount * MapGenerationTuning.globalFudgeFactor
			if resources[patch.resource].isFluid then
				deposit = Deposit.generateNewFluid(random, patch.resource, distanceFactor * resources[patch.resource].baseAmountPerWell, thisQuantity)
			else
				deposit = Deposit.generateNew(random, patch.resource, MapGenerationTuning.oneMilPatchWidth, thisQuantity)
			end 
			local position = {x=math.floor(relativePositionX*(width-deposit.width))+topLeft.x, y=math.floor(relativePositionY*(width-deposit.height))+topLeft.y}
			local chunks = deposit:chunkify(position)
			
			-- check if this deposit overlaps an existing deposit
			local overlaps = false
			for __,chunk in ipairs(chunks) do
				local overlappingChunks = cellinfo.chunks[Chunk.toIdString(chunk)]
				if overlappingChunks then
					for ___,overlappingChunk in ipairs(overlappingChunks) do
						for y=1,32,1 do
							for x=1,32,1 do
								if overlappingChunk.values[y][x] >= 0 and chunk.values[y][x] >=0 then
									overlaps=true
									break
								end
							end
							if overlaps then break end
						end
						if overlaps then break end
					end
				end
				if overlaps then break end
			end
			
			if overlaps then
				desperation = desperation+1
				if desperation > desperationGiveUpLimit then
					gaveUp = true
					break
				end
			else
				for __,chunk in ipairs(chunks) do
					local chunkId = Chunk.toIdString(chunk)
					if not cellinfo.chunks[chunkId] then
						cellinfo.chunks[chunkId] = {}
					end
					table.insert(cellinfo.chunks[chunkId], chunk)
--					table.insert(cellinfo.chunks, chunk)
				end
				break
			end
			
		end
	end
	
	if desperation>=desperationRelaxRestrictionsLimit then
		log("Reached desperation factor "..desperation.." when generating cell "..cell.ring..", "..cell.cellnum)
	end
	if gaveUp then
		log("Failed to place some resources when generating cell "..cell.ring..", "..cell.cellnum)
	end
	
	
	cellinfo.status = MapManager.Status.GENERATED
	
	return cellinfo
end


function MapManager.patchToString(patch)
	local locationText
	if patch.location==MapManager.Location.NEAR then locationText="NEAR"
	elseif patch.location==MapManager.Location.FAR then locationText="FAR"
	elseif patch.location==MapManager.Location.CENTER then locationText="CENTER"
	else locationText="NOT_CENTER" end
	return patch.resource..": "..patch.amount.." ("..locationText..")"
end


function MapManager.toString(mapMan)
	local lines = {}
	table.insert(lines, "Starting Area:")
	for _,patch in ipairs(mapMan.rings[0].cellinfos[1].patches) do
		table.insert(lines, "    "..MapManager.patchToString(patch))
	end
	for i = 1,#mapMan.rings,1 do
		table.insert(lines, "Ring Level "..i..":")
		for cellnum,cellinfo in ipairs(mapMan.rings[i].cellinfos) do
			table.insert(lines, "  Cell "..cellnum..":")
			for __,patch in ipairs(cellinfo.patches) do
				table.insert(lines, "    "..MapManager.patchToString(patch))
			end
		end
	end
	return table.concat(lines, "\n")
end







