require "danlib.FactorioUtils"

-- color is for testing and is optional
-- baseDensity is the average amount of resources per chunk (32x32 tiles) in and around starting area
-- frequency is the average number of patches per ring area. (see default)
-- baseAmountPerWell is the average amount in a single well in and around the starting area (defaults to normal_resource_amount)
-- isFluid is true for fluids (set by default on fluids)


ResourceUtils = {}

-- Richness: very poor = 1/2
-- Richness: very rich = 2
-- Size: very large = 6
-- Size: 

ResourceUtils.richnessMultipliers = {.5, .7, 1, 1.4, 2}
ResourceUtils.richnessMultipliers[0] = 0
ResourceUtils.sizeMultipliers = {.25, .5, 1, 2, 4}
ResourceUtils.sizeMultipliers[0] = 0
ResourceUtils.frequencyMultipliers = {.6, .8, 1, 1.4, 2}
ResourceUtils.frequencyMultipliers[0] = 1

ResourceUtils.autoplaceControlTable = {}
ResourceUtils.autoplaceControlTable["none"]=0
ResourceUtils.autoplaceControlTable["very-low"]=1
ResourceUtils.autoplaceControlTable["very-small"]=1
ResourceUtils.autoplaceControlTable["very-poor"]=1
ResourceUtils.autoplaceControlTable["low"]=2
ResourceUtils.autoplaceControlTable["small"]=2
ResourceUtils.autoplaceControlTable["poor"]=2
ResourceUtils.autoplaceControlTable["normal"]=3
ResourceUtils.autoplaceControlTable["medium"]=3
ResourceUtils.autoplaceControlTable["regular"]=3
ResourceUtils.autoplaceControlTable["high"]=4
ResourceUtils.autoplaceControlTable["big"]=4
ResourceUtils.autoplaceControlTable["good"]=4
ResourceUtils.autoplaceControlTable["very-high"]=5
ResourceUtils.autoplaceControlTable["very-big"]=5
ResourceUtils.autoplaceControlTable["very-good"]=5

function ResourceUtils.coverageFunction(coverage)
	-- found experimentally
	-- what is this madness?
	local density = math.exp(-19890*coverage*coverage + 939*coverage - 1.776)
	return density
end

ResourceUtils.SettingNames={"baseDensity", "frequency", "baseAmountPerWell", "isFluid"}

function ResourceUtils.generateResourceSet(surface, remotes)
	local mps = surface.map_gen_settings
	
	local resources = {}
	
	for name, prototype in pairs(game.entity_prototypes) do
	
		local mapGenAutoplaceInfo = surface.map_gen_settings.autoplace_controls[name]
		local resource
		if prototype and prototype.type=="resource" and prototype.autoplace_specification then
			resource = {
				baseDensity=ResourceUtils.coverageFunction(game.entity_prototypes[name].autoplace_specification.coverage)
			}
			resource.frequency = 1
			if game.entity_prototypes[name].resource_category == "basic-fluid" or game.entity_prototypes[name].resource_category == "water" then
				resource.baseAmountPerWell = game.entity_prototypes[name].normal_resource_amount
				resource.isFluid = true
				resource.baseDensity = resource.baseDensity * 36791
				--resource.baseDensity = resource.baseDensity * game.entity_prototypes[name].autoplace_specification.richness_base/6.5
			else
				resource.baseDensity = resource.baseDensity * 8311
				--resource.baseDensity = resource.baseDensity * (game.entity_prototypes[name].autoplace_specification.richness_multiplier_distance_bonus+10)*300
			end
			
			for modName, remoteResources in pairs(remotes) do
				if remoteResources[name] then
					for _,settingName in ipairs(ResourceUtils.SettingNames) do
						if remoteResources[name][settingName] then resource[settingName] = remoteResources[name][settingName] end
					end
					log("Using settings from "..modName.." for resource "..name)
				end
			end
			
			if mapGenAutoplaceInfo then
				local richnessMultiplier = ResourceUtils.richnessMultipliers[ResourceUtils.autoplaceControlTable[mapGenAutoplaceInfo.richness]]
				resource.baseDensity = resource.baseDensity*richnessMultiplier
				
				local sizeMultiplier = ResourceUtils.sizeMultipliers[ResourceUtils.autoplaceControlTable[mapGenAutoplaceInfo.size]]
				resource.baseDensity = resource.baseDensity*sizeMultiplier
				
				local frequencyMultiplier = ResourceUtils.frequencyMultipliers[ResourceUtils.autoplaceControlTable[mapGenAutoplaceInfo.frequency]]
				resource.frequency = resource.frequency*frequencyMultiplier
			end
			
			resources[name] = resource
		end
		
	end
	
	return resources
	
end


