require "danlib.FactorioUtils"

-- Original radar as of 15.31
--data:extend({{
--    type = "radar",
--    name = "radar-stupid",
--    icon = "__base__/graphics/icons/radar.png",
--    flags = {"placeable-player", "player-creation"},
--    minable = {hardness = 0.2, mining_time = 0.5, result = "radar-stupid"},
--    max_health = 250,
--    corpse = "big-remnants",
--    resistances =
--    {
--      {
--        type = "fire",
--        percent = 70
--      },
--      {
--        type = "impact",
--        percent = 30
--      }
--    },
--    collision_box = {{-1.4, -1.4}, {1.4, 1.4}},
--    selection_box = {{-1.5, -1.5}, {1.5, 1.5}},
--    energy_per_sector = "10MJ",
--    max_distance_of_sector_revealed = 14,
--    max_distance_of_nearby_sector_revealed = 3,
--    energy_per_nearby_scan = "250kJ",
--    energy_source =
--    {
--      type = "electric",
--      usage_priority = "secondary-input"
--    },
--    energy_usage = "300kW",
--    pictures =
--    {
--      filename = "__base__/graphics/entity/radar/radar.png",
--      priority = "low",
--      width = 153,
--      height = 131,
--      apply_projection = false,
--      direction_count = 64,
--      line_length = 8,
--      shift = util.by_pixel(27.5,-12.5)
--    },
--    vehicle_impact_sound =  { filename = "__base__/sound/car-metal-impact.ogg", volume = 0.65 },
--    working_sound =
--    {
--      sound = {
--        {
--          filename = "__base__/sound/radar.ogg"
--        }
--      },
--      apparent_volume = 2,
--    },
--    radius_minimap_visualisation_color = { r = 0.059, g = 0.092, b = 0.235, a = 0.275 },
--  }})
--  {
--    type = "item",
--    name = "radar",
--    icon = "__base__/graphics/icons/radar.png",
--    flags = {"goes-to-quickbar"},
--    subgroup = "defensive-structure",
--    order = "d[radar]-a[radar]",
--    place_result = "radar",
--    stack_size = 50
--  },
--  {
--    type = "recipe",
--    name = "radar",
--    ingredients =
--    {
--      {"electronic-circuit", 5},
--      {"iron-gear-wheel", 5},
--      {"iron-plate", 10}
--    },
--    result = "radar",
--    requester_paste_multiplier = 4
--  },


local radar2entity = cloneEntityPrototype(data.raw["radar"]["radar"], "radar-adv", {r=1, g=.8, b=.4})
radar2entity.energy_per_sector = "20MJ" -- orig "10MJ"
radar2entity.max_distance_of_sector_revealed = 28 -- orig 14
radar2entity.energy_usage = "600kW" -- orig "300kW"

local radar2item = cloneEntityPrototype(data.raw["item"]["radar"], "radar-adv", {r=1, g=.8, b=.4})
radar2item.order = "d[radar]-b[radar-adv]"

local radar2recipe = cloneEntityPrototype(data.raw["recipe"]["radar"], "radar-adv", {r=1, g=.8, b=.4})
radar2recipe.enabled = false
radar2recipe.ingredients = {
      {"advanced-circuit", 5},
      {"iron-gear-wheel", 5},
      {"iron-plate", 10} }
      
local radar2technology = {
    type = "technology",
    name = "radar-adv",
    icon="__prettyfairresources__/graphics/radar-tech.png",
    icon_size=128,
    effects =
    {
      {
        type = "unlock-recipe",
        recipe = "radar-adv"
      }
    },
    prerequisites = {"advanced-electronics"},
    unit =
    {
      count = 100,
      ingredients =
      {
        {"science-pack-1", 1},
        {"science-pack-2", 1}
      },
      time = 15
    },
    order = "e-a-b"
  }
      

data:extend({radar2entity, radar2item, radar2recipe, radar2technology})


local radar3entity = cloneEntityPrototype(data.raw["radar"]["radar"], "radar-super", {r=.8, g=.6, b=.7})
radar3entity.energy_per_sector = "20MJ" -- orig "10MJ"
radar3entity.max_distance_of_sector_revealed = 56 -- orig 14
radar3entity.energy_usage = "600kW" -- orig "300kW"

local radar3item = cloneEntityPrototype(data.raw["item"]["radar"], "radar-super", {r=.8, g=.6, b=.7})
radar3item.order = "d[radar]-c[radar-super]"

local radar3recipe = cloneEntityPrototype(data.raw["recipe"]["radar"], "radar-super", {r=.8, g=.6, b=.7})
radar3recipe.enabled = false
radar3recipe.ingredients = {
      {"processing-unit", 5},
      {"iron-gear-wheel", 5},
      {"iron-plate", 10} }

local radar3technology = {
    type = "technology",
    name = "radar-super",
    icon="__prettyfairresources__/graphics/radar-tech.png",
    icon_size=128,
    effects =
    {
      {
        type = "unlock-recipe",
        recipe = "radar-super"
      }
    },
    prerequisites = {"radar-adv", "advanced-electronics-2"},
    unit =
    {
      count = 150,
      ingredients =
      {
        {"science-pack-1", 1},
        {"science-pack-2", 1},
        {"science-pack-3", 1}
      },
      time = 30
    },
    order = "e-a-b"
  }

data:extend({radar3entity, radar3item, radar3recipe, radar3technology})






