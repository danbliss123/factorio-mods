
Modifies the resource generation to fit a wider variety of playstyles without changing game balance.
This mod makes resource patches much larger and less frequent as you go further from the origin.
These two effects balance eachother out to produce the same average amount of resources per land area.
This way, expanding in the late game does not require tediously mining out patch after patch.

Also includes two levels of long-range radar to help you explore in the late game.

I made this because I got very frustrated trying to mine out large areas in the super-late game.  There are just so many patches to mine.
I know I can set resources to very-big, and that would solve my problem.  That would also make the early game too easy, though.  I wanted a standard early
game that would transition into a late game that did not force me to mine out a bajillion patches.

Also, I do not like the idea of choosing between rail-oriented map and non-rail-oriented map at map generation time.
With PFR, the map starts out non-rail oriented, and transitions to more and more rail oriented as patches become farther apart.


Specifics:

* Near the origin, the map will look very similar to what you would get in vanilla.

* As you get farther from the origin (starting at around 500 tiles = 2 screens), patch size starts growing significantly while the number of patches starts dropping significantly.
** All of this is balanced to give the same average density at any distance as you would get without PFR.

* PFR resources will actually overwrite water, meaning land will be created under the resource if it happens to end up in water.
** This adds resource islands to the game, which I am curious to hear if people like.
** Note that landfill is part of the game now, so getting to the islands should not be a problem.
** PFR will never place resources on top of eachother.

* PFR reads and obeys the map generation settings from the map generation screen. So, for example, it will obey if you set stone to "very-good" richness.
** This also means that the presets like "Rail World" still work with PFR.

* PFR can be turned on or off on a per-map basis.  You can do this under mod settings on the map generation screen.
** This does NOT require restarting the game!
** Pre-existing maps will have PFR off, so your old maps will be unaffected by installing PFR.
** PFR can be activated on old maps if you want.  Open the command line with ~ and run /pfr_regen.  Note that this will wipe out all existing patches!
** PFR can be used with other resource spawn mods like RSO, as long as it is turned off.  RSO cannot be turned off so it needs to be uninstalled to use PFR.

* PFR will generate all resources, including modded resources.
** The algorithm for determining placement characteristics for modded resources is clumsy, (sorry) but it works pretty well. (or pretty fair?)
*** It works well for bob's ores, but I cannot guarantee it will work out of the box for every mod.  Let me know in the forums if PFR generates a modded resource wrong.
** Mods can specify to PFR what their generation characteristics should be like.  See the FAQ below.
** Any player can specify what the placement of any resource should be like, if you want.  All you need is a text editor.  See the FAQ below.

* PFR obeys the general design principles of Factorio map generation.  In particular:
** Maps generated with the same seed will be the same.
** Map generation is the same no matter in what order you explore the map. (A surprisingly difficult principle to adhere to for infinite maps)



FAQ:

================================================================================================================
I want to tweak the resource generation for my game and the map gen settings are not enough for me.  What can I do?
================================================================================================================

The file you are looking for is resourceCustomization.lua
It is typically in C:\Users\yourself\AppData\Roaming\Factorio\mods\prettyfairresources-...zip
You will need to unzip the archive to edit it.
Comments in that file explain what you need to do in more detail.



================================================================================================================
My mod has resources, but PFR generates them wrong.  How can I fix this?
================================================================================================================

Your mod can specify resource settings directly to PFR.  You do this by creating a
remote interface and giving it a function called get-pfr-resources.  This should then return
an object similar to the object in resourceCustomization.lua.
PFR scans all of the interfaces and calls every get-pfr-resources function it finds.
The settings returned this way will override PFR's automatically determined settings.

tl;dr put this into your control.lua:
remote.add_interface("myModName",
{
  ["get-pfr-resources"] = function()
	return {
		["sludge-deposit"] = {baseDensity = 8500}
	}
  end
}


================================================================================================================
How does PFR map generation work?
================================================================================================================

I may write up a full explanation at some point.  In the mean time, the best way to understand is to
look at the file patchLocationHelp.txt, which explains the coordinate system.  Then look at the log produced
by a call to /pfr_log_map_cheats.



