-- This file is for manually tweaking the resource generation in PFR.
-- Add the resources you want to tweak to the PresetResources object.

-- PresetResources is a dictionary of resource name to resource PFR settings.
-- resource PFR settings are:
-- baseDensity = average number of resources per chunk in or around the starting area (chunk is 32x32 tiles)
-- frequency = average number of patches in a ring (see patchLocationHelp.txt)
-- baseAmountPerWell = average resource amount per well (for fluids, obviously) in or around the starting area
-- isFluid = true for fluids

PresetResources = {
-- Examples for base game:
-- Any field can be omitted to use the default
--	["coal"]=		{baseDensity=8500, frequency=1},
--	["copper-ore"]=	{baseDensity=8500, frequency=1},
--	["crude-oil"]=	{baseDensity=8500, frequency=1, baseAmountPerWell=300000, isFluid=true},
--	["iron-ore"]=	{baseDensity=8500, frequency=1},
--	["stone"]=		{baseDensity=3000, frequency=1},
--	["uranium-ore"]={baseDensity=350 , frequency=1},
}

-- Note that this file will get overwritten when this mod is upgraded.
-- If you make changes, back them up before you upgrade.

-- Tuning characteristics for the map generator.
MapGenerationTuning = {
	-- Number of patches per ring, by default
	defaultPatchCount = 6,
	-- Inverse of the slope of the resource bonus curve
	bonusDistance=500,
	-- Width of a 1 million amount patch (approx)
	oneMilPatchWidth = 25,
	-- max ratio between large and small patches at the same distance
	patchSizeMaxRatio=2,
	-- for randomly choosing cells.  corner cells should be listed less frequently
	cellIdTable = {1,2,2,3,3,4,5,5,6,6,7,8,8,9,9,10,11,11,12,12},
	-- Amount to increase desperation on each failure
	ringDesperationFactor = .2,
	-- Minimum size factor to support the starting area
	minSizeFactor = 1600,
	-- Factor that all richness is multiplied by
	globalFudgeFactor = .38,
	-- Size of "CENTER" area relative to 1 being the cell width (i.e. starting area width)
	centerAreaWidth = .33,
}



