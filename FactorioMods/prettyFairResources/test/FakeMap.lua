require "danlib.Chunk"
require "danlib.Map"
require "pfr.MapManager"
require "img.XpmLib"
require "pfr.Resources"
require "FactorioTest"

FakeMap = {
	radius=0,
	data={},
	resourceTable={}
}


FakeMapResources = {}
FakeMapResources.base = {}
FakeMapResources.base["coal"]=			{color="000000", baseDensity=8501.5, frequency=1}
FakeMapResources.base["copper-ore"]=	{color="CC6236", baseDensity=8501.5, frequency=1}
FakeMapResources.base["crude-oil"]=		{color="C633C4", baseDensity=8501.5, frequency=1, baseAmountPerWell=300000, isFluid=true}
FakeMapResources.base["iron-ore"]=		{color="556A6C", baseDensity=8501.5, frequency=1}
FakeMapResources.base["stone"]=			{color="797250", baseDensity=2524.3, frequency=1}
FakeMapResources.base["uranium-ore"]=	{color="00B200", baseDensity=277.27, frequency=1}

local ridiculousPatchCount = 1
local ridiculousPatchDensity = 17000

FakeMapResources.ridiculous = {}
FakeMapResources.ridiculous["coal"]=		{color="000000", baseDensity=8500 , frequency=ridiculousPatchCount}
FakeMapResources.ridiculous["copper-ore"]=	{color="CC6236", baseDensity=8500 , frequency=ridiculousPatchCount}
FakeMapResources.ridiculous["crude-oil"]=	{color="C633C4", baseDensity=8500 , frequency=ridiculousPatchCount, baseAmountPerWell=300000, isFluid=true}
FakeMapResources.ridiculous["iron-ore"]=	{color="556A6C", baseDensity=8500 , frequency=ridiculousPatchCount}
FakeMapResources.ridiculous["stone"]=		{color="797250", baseDensity=3000 , frequency=ridiculousPatchCount}
FakeMapResources.ridiculous["uranium-ore"]=	{color="00B200", baseDensity=350  , frequency=ridiculousPatchCount}
FakeMapResources.ridiculous["goop1"]=		{color="ff0000", baseDensity=ridiculousPatchDensity , frequency=ridiculousPatchCount}
FakeMapResources.ridiculous["goop2"]=		{color="e02000", baseDensity=ridiculousPatchDensity , frequency=ridiculousPatchCount}
FakeMapResources.ridiculous["goop3"]=		{color="c04000", baseDensity=ridiculousPatchDensity , frequency=ridiculousPatchCount}
FakeMapResources.ridiculous["goop4"]=		{color="a06000", baseDensity=ridiculousPatchDensity , frequency=ridiculousPatchCount}
FakeMapResources.ridiculous["goop5"]=		{color="808000", baseDensity=ridiculousPatchDensity , frequency=ridiculousPatchCount}
FakeMapResources.ridiculous["goop6"]=		{color="60a000", baseDensity=ridiculousPatchDensity , frequency=ridiculousPatchCount}
FakeMapResources.ridiculous["goop7"]=		{color="40c000", baseDensity=ridiculousPatchDensity , frequency=ridiculousPatchCount}
FakeMapResources.ridiculous["goop8"]=		{color="20e000", baseDensity=ridiculousPatchDensity , frequency=ridiculousPatchCount}
FakeMapResources.ridiculous["goop9"]=		{color="00ff00", baseDensity=ridiculousPatchDensity , frequency=ridiculousPatchCount}
FakeMapResources.ridiculous["goop10"]=		{color="00e020", baseDensity=ridiculousPatchDensity , frequency=ridiculousPatchCount}
FakeMapResources.ridiculous["goop11"]=		{color="00c040", baseDensity=ridiculousPatchDensity , frequency=ridiculousPatchCount}
FakeMapResources.ridiculous["goop12"]=		{color="00a060", baseDensity=ridiculousPatchDensity , frequency=ridiculousPatchCount}
FakeMapResources.ridiculous["goop13"]=		{color="008080", baseDensity=ridiculousPatchDensity , frequency=ridiculousPatchCount}
FakeMapResources.ridiculous["goop14"]=		{color="0060a0", baseDensity=ridiculousPatchDensity , frequency=ridiculousPatchCount}
FakeMapResources.ridiculous["goop15"]=		{color="0040c0", baseDensity=ridiculousPatchDensity , frequency=ridiculousPatchCount}
FakeMapResources.ridiculous["goop16"]=		{color="0020e0", baseDensity=ridiculousPatchDensity , frequency=ridiculousPatchCount}
FakeMapResources.ridiculous["goop17"]=		{color="0000ff", baseDensity=ridiculousPatchDensity , frequency=ridiculousPatchCount}
FakeMapResources.ridiculous["goop18"]=		{color="2000e0", baseDensity=ridiculousPatchDensity , frequency=ridiculousPatchCount}
FakeMapResources.ridiculous["goop19"]=		{color="4000c0", baseDensity=ridiculousPatchDensity , frequency=ridiculousPatchCount}
FakeMapResources.ridiculous["goop20"]=		{color="6000a0", baseDensity=ridiculousPatchDensity , frequency=ridiculousPatchCount}
FakeMapResources.ridiculous["goop21"]=		{color="800080", baseDensity=ridiculousPatchDensity , frequency=ridiculousPatchCount}
FakeMapResources.ridiculous["goop22"]=		{color="a00060", baseDensity=ridiculousPatchDensity , frequency=ridiculousPatchCount}
FakeMapResources.ridiculous["goop23"]=		{color="c00040", baseDensity=ridiculousPatchDensity , frequency=ridiculousPatchCount}
FakeMapResources.ridiculous["goop24"]=		{color="e00020", baseDensity=ridiculousPatchDensity , frequency=ridiculousPatchCount}


FakeMap.__index = FakeMap

function FakeMap.new(radius, resources)
	local obj = {radius=radius, resourceTable={}}
	setmetatable(obj, FakeMap)
	
	local tableSize = math.ceil((radius/32)*math.sqrt(2))
	
	for name, info in pairs(resources) do
		obj.resourceTable[name] = {}
		for i=1,tableSize,1 do
			obj.resourceTable[name][i]=0
		end
		if info.isFluid then
			local fluidName = name.."-count"
			obj.resourceTable[fluidName] = {}
			for i=1,tableSize,1 do
				obj.resourceTable[fluidName][i]=0
			end
		end
	end
	
	obj.data = Map.makeMapRaster(radius)
	
	return obj
end

function FakeMap:drawChunks(chunks, resourceToColorIndex)
	
	for chunkId,overlappingChunks in pairs(chunks) do
		for _,chunk in ipairs(overlappingChunks) do
			-- chunk.x chunk.y chunk.resource chunk[]
			local topLeft = Chunk.toTopLeft(chunk)
			local color = resourceToColorIndex[chunk.resource]
			
			for r, row in ipairs(chunk.values) do
				local my = topLeft.y+r-.5
				local dy = math.floor(my)+self.radius+1
				for c, value in ipairs(row) do
					local mx = topLeft.x+c-.5
					local dx = math.floor(mx)+self.radius+1
					local d = math.ceil(math.sqrt(mx*mx+my*my)/32)
					if (value>0) then
						self.data[dy][dx] = color
						self.resourceTable[chunk.resource][d] = self.resourceTable[chunk.resource][d] + value
						local fluidName = chunk.resource.."-count"
						if self.resourceTable[fluidName] then
							self.resourceTable[fluidName][d] = self.resourceTable[fluidName][d] + 1
						end
					elseif value==0 then
						self.data[dy][dx] = 3
					end
				end
			end
		end
	end

end



function makeAFakeMap()

	local fakeMapMaxRing = 3
	local fakeMapRadius = 256*math.pow(2,fakeMapMaxRing)

	local osTime = os.time()
	print ("Random seed = "..osTime)
	math.randomseed( osTime )
	print ("Initializing map")
	
	fakeMapResources = FakeMapResources.base
	local fakeMap = FakeMap.new(fakeMapRadius, fakeMapResources)
	
	local resourceIndexes= {}
	local resourceColors={"FFFFFF","DDDDDD","999999"}
	
	local rindex = 4
	for name,info in pairs(fakeMapResources) do
		resourceIndexes[name] = rindex
		table.insert(resourceColors,info.color)
		rindex = rindex+1
	end
	
	
	local mapMan = MapManager.new(fakeMapResources)
	
	print ("Generating rings")
	
	MapManager.generateUpTo(mapMan,math.random,fakeMapMaxRing)
	
	print ("Generating and drawing cells")
	
	if mapMan.rings[0] then
		local cellinfo = MapManager.generateCell(math.random,{ring=0, cellnum=1},mapMan.rings[0].cellinfos[1], fakeMapResources)
		fakeMap:drawChunks(cellinfo.chunks, resourceIndexes )
	end
	
	for r,ring in ipairs(mapMan.rings) do
		print("Ring "..r)
		for c,cellinfo in ipairs(ring.cellinfos) do
			cellinfo = MapManager.generateCell(math.random,{ring=r, cellnum=c},cellinfo, fakeMapResources)
			fakeMap:drawChunks(cellinfo.chunks, resourceIndexes )
		end
	end
	
	
	printResourceTable(fakeMap.resourceTable)
	
	return fakeMap.resourceTable
	
--	print ("Writing file")
--	
--	XpmLib.writeToFile({raster=fakeMap.data, colors=resourceColors},"testxpm.xpm")
--	
--	print ("Done")
--	
--	print(MapManager.toString(mapMan))
	

end

function printResourceTable(rtable)
	local headerRow = {"name"}
	for i = 4,#rtable["coal"],1 do
		local value = i*32
		table.insert(headerRow, value)
	end
	print(table.concat(headerRow,","))
	for resource, rTable in pairs(rtable) do
		local row = {resource}
		local total=0
		for i = 1,#rTable,1 do
			local value = rTable[i]
			total = total + value
			if i>=4 then
				table.insert(row, total)
			end
		end
		print(table.concat(row,","))
	end
end

function averageResourceTables(tables)
	local resources = {}
	for rname,_ in pairs(tables[1]) do
		resources[rname] = {}
	end
	for _,t in ipairs(tables) do
		for rname,resource in pairs(resources) do
			for i,value in ipairs(t[rname]) do
				resource[i] = (resource[i] or 0) + t[rname][i]
			end
		end
	end
	local count = #tables
	for rname,resource in pairs(resources) do
		for i,_ in ipairs(resource) do
			resource[i] = resource[i]/count
		end
	end
	return resources
end

function analyzeNMaps(n)
	local results = {}
	for i = 1,n,1 do
		table.insert(results, makeAFakeMap())
	end
	printResourceTable(averageResourceTables(results))
end

analyzeNMaps(10)



