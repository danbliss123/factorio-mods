require "pfr.Blob"
require "img.XpmLib"

--cBlob = ComplexBlob.create( {
--	SimpleBlob.fromParameters(4,4,0,0,0,1),
--	SimpleBlob.fromParameters(2,1,1,2,3,.5),
--	SimpleBlob.fromParameters(2,1,2,-2,2,.5),
--	SimpleBlob.fromParameters(2,1,3,2,-1.5,-.5),
--	--SimpleBlob.fromParameters(a,b,th,x,y,z),
--
--}, .5)

local cBlobs = {}

local blobCountY = 8
local blobCountX = 16
local blobWidth = 100
local blobGap = 10

for y = 1,blobCountY,1 do
	cBlobs[y] = {}
	for x = 1,blobCountX,1 do
		cBlobs[y][x] = ComplexBlob.generate(math.random, ComplexBlob.defaultProfiles)
	end
end


print(  ComplexBlob.toString(cBlobs[1][1]) )

local raster = {}
local width = ((blobWidth+blobGap)*blobCountX) + blobGap
local height = ((blobWidth+blobGap)*blobCountY) + blobGap

for y=1,height,1 do
	raster[y] = {}
	for x=1,width,1 do
		raster[y][x] = 1
	end
end

for y = 1,blobCountY,1 do
	for x = 1,blobCountX,1 do
		ComplexBlob.draw(cBlobs[y][x],raster, blobGap+1+(y-1)*(blobWidth+blobGap), blobGap+1+(x-1)*(blobWidth+blobGap), blobWidth,blobWidth,3,4,2)
	end
end

print ("Writing XPM")

XpmLib.writeToFile({raster=raster, colors={"000000", "222222", "00FF00", "105555"}}, "blob.xpm")
