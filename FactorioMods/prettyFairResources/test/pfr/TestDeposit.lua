require "pfr.Deposit"
require "test.Test"


TestDeposit = {
	testDepositGeneration = ParameterizedTest.new({
		depositSelfTest = function (values)
			return Deposit.generateNew(math.random, "iron",values.concentration,values.amount):selfTest()
		end,
		values = {
			{concentration=100, amount=1000000},
			{concentration=50, amount=1000000},
			{concentration=20, amount=1000000},
			{concentration=150, amount=1000000},
			{concentration=23, amount=5000000},
			{concentration=99, amount=34},
			{concentration=101, amount=100000000}
		}
	}),

	testOneDeposit = function()
		return Deposit.generateNew(math.random,"iron",100,1000000):selfTest()
	end
}

